class PlayersController < ApplicationController
	before_filter :authenticate_user!
	before_filter :admin_user, only: [:new, :edit, :create,
																		:update, :destroy]

  def index
    @players = Player.search(params[:search]).order("appearances DESC")
  end

	def show
  	@player = Player.find(params[:id])
    @team   = @player.team_id
    @reviews = @player.reviews
	end

  def new
    @player = Player.new
    @player.team_id = params[:team_id]
  end

  def create
    @player = Player.create(params[:player])

    if @player.save
      flash[:success] = "Player added"
      redirect_to @player
    else
      render :action => "new"
    end
  end

	def edit
		@player = Player.find(params[:id])
	end

	def update
    @player = Player.find(params[:id])

    respond_to do |format|
      if @player.update_attributes(params[:player])
        format.html { redirect_to @player, notice: 'Player was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end  	
	end

	def destroy
  	@player = Player.find(params[:id])
  	@player.destroy
    @team = @player.team_id

    redirect_to team_path(@team)
	end
  
end