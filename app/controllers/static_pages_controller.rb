class StaticPagesController < ApplicationController
  before_filter :authenticate_user!, except: [:under_development, :help]
  before_filter :admin_user, only: :dashboard
  
  def home
    @users = User.all
  	@user = current_user
  	
    if (params[:gameweek])
      @matches = Match.search(params[:gameweek]).order("kickoff DESC")
    else
      @matches = Match.order("kickoff DESC")
    end

    @match_options = Match.all.map{ |m| [m.id, m.gameweek] }
    @fantasy_players = current_user.fantasyteam.all
    @players = Player.paginate(page: params[:page], per_page: 15).order("created_at DESC")
    @teams = Team.order('league_points DESC, goal_difference DESC')

    respond_to do |format|
      format.html
      format.js
    end
  end

  def help
  end

  def under_development
  end

  def dashboard
    @users = User.paginate(page: params[:page], per_page: 5)
    @paying_users = Subscription.all
  end

end
