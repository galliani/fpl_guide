class TeamStatsController < ApplicationController
	before_filter :admin_user, only: [:new, :create, :edit, :update, :destroy]

	def new
	  	@match = Match.find(params[:match_id])
	  	@hometeam = @match.hometeam
	  	@awayteam = @match.awayteam
		@team_stat = @match.team_stats.build
	end

	def create
		@match = Match.find(params[:match_id])
		@team_stat = @match.team_stats.build(params[:team_stat])
	    if @team_stat.save
	      flash[:success] = "Team Stats for this match posted"
	      redirect_to match_path(@match)
	    else
	      flash[:alert] = "Team Stats not posted"
	      render action: 'new'
	    end		
	end

	def edit
	  	@match = Match.find(params[:match_id])
	  	@hometeam = @match.hometeam
	  	@awayteam = @match.awayteam
		@team_stat = TeamStat.find(params[:id])
	end

	def update
		@match = Match.find(params[:match_id])
		@team_stat = TeamStat.find(params[:id])

		if @team_stat.update_attributes(params[:team_stat])
			redirect_to @match
			flash[:success] = "Team Stats for was successfully updated"
		else
			render action: 'edit'
		end
	end

	def destroy
		@match = Match.find(params[:match_id])
		@team_stat = TeamStat.find(params[:id])
		@team_stat.destroy

		redirect_to match_path(@match)
	end

	def edit_all
	  	@match = Match.find(params[:match_id])
	  	@hometeam = @match.hometeam
	  	@awayteam = @match.awayteam
		@team_stats = @match.team_stats.all
	end

	def update_all
		@match = Match.find(params[:match_id])

	  params['team_stat'].keys.each do |id|
	    @team_stat = @match.team_stats.find(id.to_i)
	    @team_stat.update_attributes(params['team_stat'][id])
	  end

	  redirect_to match_path(@match)
	end
end