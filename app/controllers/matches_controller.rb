class MatchesController < ApplicationController
	before_filter :authenticate_user!
	before_filter :admin_user, only: [:new, :create, :edit, :update, :destroy]

  def index
    @matches = Match.where(nil)
    @matches = @matches.gameweek(params[:gameweek]) if params[:gameweek].present?
  end

	def show
		@match = Match.find(params[:id])
    @homeplayers = @match.hometeam.players
    @awayplayers = @match.awayteam.players
    @squad_player = @match.squad_players.build
    @squad_players = @match.squad_players.all
    @home_stat = TeamStat.find_by_match_id_and_team_id(@match, @match.hometeam_id)
    @away_stat = TeamStat.find_by_match_id_and_team_id(@match, @match.awayteam_id)
	end

	def new
		@match = Match.new
	end

	def create
		@match = Match.new(params[:match])

  	respond_to do |format|
      if @match.save
        format.html { redirect_to @match, notice: 'Match was successfully created.' }
        format.json { render json: @match, status: :created, location: @match }
      else
        format.html { render action: "new" }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
	end

	def edit
		@match = Match.find(params[:id])
	end

	def update
		@match = Match.find(params[:id])

    respond_to do |format|
      if @match.update_attributes(params[:match])
        format.html { redirect_to match_path(@match), notice: 'Match was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end		
	end

	def destroy
		@match = Match.find(params[:id])
		@match.destroy

  	respond_to do |format|
  		format.html { redirect_to user_root_path }
  		format.json { head :no_content }
  	end
	end

end
