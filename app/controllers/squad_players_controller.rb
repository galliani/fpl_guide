class SquadPlayersController < ApplicationController
	before_filter :admin_user

	def create
		@match = Match.find(params[:match_id])
		@squad_player = @match.squad_players.build(params[:squad_player])

		if @squad_player.save
			flash.now[:success] = ["Chosen for selection"]
			respond_to do |format|
				format.html { redirect_to match_path(@match) }
				# format.js
			end									
		else
			flash.now[:alert] = ["Failed to choose"]
			respond_to do |format|
				format.html { redirect_to match_path(@match) }
				# format.js
			end									
		end
	end

	def edit
		@match = Match.find(params[:match_id])
		@squad_player = SquadPlayer.find(params[:id])
	end

	def update
		@match = Match.find(params[:match_id])
		@squad_player = SquadPlayer.find(params[:id])
      
    respond_to do |format|
	      if @squad_player.update_attributes(params[:squad_player])
	        format.html { redirect_to match_path(@match), notice: 'The Squad Player was successfully updated.' }
	      else
	        format.html { render action: "edit" }
	      end
	  end
	end

	def destroy
		@match = Match.find(params[:match_id])
		@squad_player = SquadPlayer.find(params[:id])
		@squad_player.destroy
		flash.now[:info] = ["Recalled"]

		respond_to do |format|
			format.html { redirect_to match_path(@match) }
			# format.js
		end				
	end

	def edit_all
		@match = Match.find(params[:match_id])
		@squad_players = @match.squad_players.all
		# @awayplayers = @match.awayteam.players
	end

	def update_all
		@match = Match.find(params[:match_id])

	  params['squad_player'].keys.each do |id|
	    @squad_player = @match.squad_players.find(id.to_i)
	    @squad_player.update_attributes(params['squad_player'][id])
	    # Player.find_by_player_id(@squad_player.player_id).reviews.create!(rating: @squad_player.rating)
	  end

	  redirect_to match_path(@match)
	end

end
