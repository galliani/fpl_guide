class UsersController < ApplicationController
	before_filter :authenticate_user!

	def show
	    @user = User.find(params[:id])
    	@fantasy_players = @user.fantasyteam.all
	end
end