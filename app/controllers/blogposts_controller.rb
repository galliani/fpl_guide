class BlogpostsController < ApplicationController
  skip_before_filter :prelaunch_mode
  before_filter :admin_user, only: [:new, :create, :destroy]

  def index
    @blogposts = Blogpost.paginate(page: params[:page], per_page: 10)

    respond_to do |format|
      format.html #index.html.erb
      format.json { render json: @blogposts }
    end
  end

  def show
    @blogpost = Blogpost.find(params[:id])
    @previous = Blogpost.where("id < ?", params[:id]).order(:id).first   
    @next = Blogpost.where("id > ?", params[:id]).order(:id).first 

    respond_to do |format|
      format.html #show.html.erb
      format.json { render json: @blogpost }
    end
  end

  def new
    @blogpost = Blogpost.new

    respond_to do |format|
      format.html #new.html.erb
      format.json { render json: @blogpost }
    end
  end

  def edit
    @blogpost = Blogpost.find(params[:id])
  end

  def create
    @blogpost = Blogpost.new(params[:blogpost])

    respond_to do |format|
      if @blogpost.save
        format.html { redirect_to @blogpost, notice: 'Blogpost was successfully created.' }
        format.json { render json: @blogpost, status: :created, location: @blogpost }
      else
        format.html { render action: "new" }
        format.json { render json: @blogpost.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @blogpost = Blogpost.find(params[:id])

    respond_to do |format|
      if @blogpost.update_attributes(params[:blogpost])
        format.html { render action: "show" , notice: 'Blogpost was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @blogpost.errors, status: :unprocessable_entity }
      end
    end       
  end

  def destroy
    @blogpost = Blogpost.find(params[:id])
    @blogpost.destroy

    respond_to do |format|
      format.html { redirect_to blogposts_path }
      format.json { head :no_content }
    end
  end

  private

  def admin_user
    redirect_to root_path unless current_user.admin?
  end

end
