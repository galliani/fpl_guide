class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :prelaunch_mode
	include SubscriptionsHelper
  # helper_method :subscription_exists?, :find_subscription 

  def after_sign_in_path_for(resource)
  	user_root_path
  end

  def after_sign_out_path_for(resource)
  	root_path
  end

  def after_sign_up_path_for(user)
  	after_sign_in_path_for(user)
  end
 
  def prelaunch_mode
    if Rails.env.production? 
      redirect_to under_development_path unless current_user.try(:admin?)
      flash[:success] = "Please be patient, it is still under development"   
    end
  end

  def admin_user
    redirect_to user_root_path unless current_user.admin?
  end

  # def paying_user 
  #   unless current_user.admin? || current_user.status = "paying"  
  #     redirect_to plans_path
  #   end
  # end


end
