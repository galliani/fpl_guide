class SubscriptionsController < ApplicationController
  before_filter :admin_user, only: [:index, :show]

  def index
    @subscriptions = Subscription.all
  end

  def show
    @subscription = Subscription.find(params[:id])
  end

  def new
    # The working one
    plan = Plan.find(params[:plan_id])
    @subscription = plan.subscriptions.build

    if params[:PayerID]
      @subscription.paypal_customer_token = params[:PayerID]
      @subscription.paypal_payment_token = params[:token]
      # @subscription.email = @subscription.paypal.checkout_details.email   
    end
  end

  def create
    # The working one
    @subscription = Subscription.new(params[:subscription])
    if @subscription.save_with_paypal_payment
      redirect_to @subscription, :notice => "Thank you for subscribing!"
    else
      redirect_to plans_path
    end  	
  end

  def paypal_checkout
    plan = Plan.find(params[:plan_id])
    subscription = plan.subscriptions.build
    redirect_to subscription.paypal.checkout_url(
      return_url: new_subscription_url(:plan_id => plan.id),
      cancel_url: root_url
    )
  end
  
  def paypal_suspend
    current_user.suspend_paypal
    redirect_to edit_user_registration_path(current_user), :notice => "We will welcome you back anytime!"
  end

  def paypal_reactivate
    current_user.reactivate_paypal
    redirect_to edit_user_registration_path(current_user), :notice => "Welcome back!"
  end

  def paypal_cancel
    current_user.cancel_paypal
    redirect_to edit_user_registration_path(current_user), :notice => "We are sorry you chose to cancel, feel free to re-sign at anytime"
  end
end
