class ReviewsController < ApplicationController
	before_filter :authenticate_user!, :admin_user

	# before_filter :correct_player, only: :destroy

	def new
  	@player = Player.find(params[:player_id])
		@review = @player.reviews.build
	end

  def create
    # Find a player in team 1 that has id=2
  	@player = Player.find(params[:player_id])
    @review = @player.reviews.build(params[:review])

    if @review.save
      flash[:success] = "Review posted"
      redirect_to player_path(@player)
    else
      flash[:alert] = "Review not posted"
      render action: 'new'
    end
  end

  def edit
    @player = Player.find(params[:player_id])
    @review = @player.reviews.find(params[:id])
  end

  def update
    @player = Player.find(params[:player_id])
    @review = Review.find(params[:id])
  
    if @review.update_attributes(params[:review])
      redirect_to @player
      flash[:success] = "Review was successfully updated"
    else
      render action: 'edit'
    end
  end

  def destroy
  	@review = Review.find(params[:id])
  	@review.destroy
  	player = @review.player_id

  	redirect_to player_path(player)
  end

end
