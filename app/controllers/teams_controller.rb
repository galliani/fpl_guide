require 'will_paginate/array'
class TeamsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :admin_user, only: [:new, :edit, :create, 
                                    :update, :destroy]
  
  def index
  	@teams = Team.order('league_points DESC, goal_difference DESC')

  	respond_to do |format|
  		format.html #index.html.erb
  		format.json { render json: @teams }
  	end
  end

  def show
  	@team = Team.find(params[:id])
    @players = @team.players
    @prev_opps = Match.get_prev_matches(@team).last(5)
    @next_opps = Match.get_next_matches(@team).first(5)
    @all_matches = Match.full_season(@team).sort!
    @all_matches = @all_matches.paginate(page: params[:page], per_page: 5)
  	# respond_to do |format|
  	# 	format.html #show.html.erb
  	# 	format.json { render json: @team }
  	# end
  end
  
  def new
  	@team = Team.new
  	
  	respond_to do |format|
  		format.html #new.html.erb
  		format.json { render json: @team }
  	end
  end

  def edit
  	@team = Team.find(params[:id])
  end

  def create
  	@team = Team.new(params[:team])

  	respond_to do |format|
      if @team.save
        format.html { redirect_to @team, notice: 'Team was successfully created.' }
        format.json { render json: @team, status: :created, location: @team }
      else
        format.html { render action: "new" }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @team = Team.find(params[:id])

    respond_to do |format|
      if @team.update_attributes(params[:team])
        format.html { redirect_to teams_path, notice: 'Team was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end  	
  end


  def destroy
  	@team = Team.find(params[:id])
  	@team.destroy

  	respond_to do |format|
  		format.html { redirect_to teams_path }
  		format.json { head :no_content }
  	end
  end

end
