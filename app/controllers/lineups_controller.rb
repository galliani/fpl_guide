class LineupsController < ApplicationController
	before_filter :authenticate_user!

	def create
		@player = Player.find(params[:lineup][:player_id])
		current_user.pick!(@player)
		flash.now[:success] = ["Sucessfully picked #{@player.name}"]		
	end

	def destroy
		@player = Lineup.find(params[:id]).player
		current_user.unpick!(@player)
		flash.now[:info] = ["Sucessfully unpicked #{@player.name}"]		
		
		respond_to do |format|
			format.html { redirect_to user_root_path }
			format.js
		end
	end

end