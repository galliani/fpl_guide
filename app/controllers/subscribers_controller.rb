class SubscribersController < ApplicationController
	skip_before_filter :prelaunch_mode
	
	def landing_page
		@subscriber = Subscriber.new
		@message = Message.new
		render layout: false
	end

	def create
		@subscriber = Subscriber.new(params[:subscriber])
		
		if @subscriber.valid?
			SubscriptionMailer.welcome(@subscriber).deliver
			flash[:success] = "Thank you, we will be in touch"
	    redirect_to root_path
	  else
	  	flash.now.alert = "Please enter a valid email address."
	  	redirect_to root_path
	  end
	end
end
