class Lineup < ActiveRecord::Base
  attr_accessible :role, :player_id, :gameweek
  belongs_to :player
  belongs_to :user

  validates :player_id, presence: true
  validates :user_id, presence: true
  
end
