class Subscriber < ActiveRecord::Base
  attr_accessible :email, :name
  validates :email, :format => { :with => %r{.+@.+\..+} }, 
  					:allow_blank => true
end
