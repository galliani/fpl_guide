class Match < ActiveRecord::Base
  scope :gameweek, order("matches.gameweek DESC")
  scope :done, ->{ where postmatch.present? }
  attr_accessible :gameweek, :kickoff, :prematch, :postmatch,
  				  :hometeam_id, :awayteam_id

  belongs_to :hometeam, class_name: 'Team', foreign_key: 'hometeam_id'
  belongs_to :awayteam, class_name: 'Team', foreign_key: 'awayteam_id'

  has_many :squad_players
  has_many :players, through: :squad_players, foreign_key: 'match_id'
  has_many :team_stats

  before_validation :clean_pre_post_match
  validates :hometeam_id, presence: true
  validates :awayteam_id, presence: true
  validates :gameweek, presence: true

  def self.search(query)
    where { (gameweek == query) }
  end

  def self.find_all_matches(team) #find all matches involving a team, home and away
    self.where{ (hometeam_id == team.id) | (awayteam_id == team.id) }.to_a
  end

  def self.get_prev_matches(team)
    self.find_all_matches(team).map do |m| 
      if m.postmatch.present? && m.hometeam_id == team.id
        TeamStat.find_by_match_id_and_team_id(m.id, m.hometeam_id)
      elsif m.postmatch.present? && m.awayteam_id == team.id
        TeamStat.find_by_match_id_and_team_id(m.id, m.awayteam_id)
      end
    end
  end

  def self.get_next_matches(team)
    self.find_all_matches(team).map do |m| 
      if m.postmatch.empty? && m.hometeam_id == team.id
        Team.find_by_id(m.awayteam_id)
      elsif m.postmatch.empty? && m.awayteam_id == team.id
        Team.find_by_id(m.hometeam_id)   
      end
    end
  end

  def self.full_season(team) 
    self.find_all_matches(team).map do |m| 
      if m.hometeam_id == team.id || m.awayteam_id == team.id
        self.find_by_id(m.id)
      end
    end
  end

  def find_squadplayer(match, player)
    SquadPlayer.find_by_match_id_and_player_id(match.id, player.id)
  end 

  private

  def clean_pre_post_match
    self.prematch = Sanitize.fragment(self.prematch, whitelist)
    self.postmatch = Sanitize.fragment(self.postmatch, whitelist)
  end

  def whitelist
    whitelist = Sanitize::Config::RELAXED
    # whitelist[:elements].push("span")
    # whitelist[:attributes]["span"] = ["style"]
    # whitelist
  end

end
