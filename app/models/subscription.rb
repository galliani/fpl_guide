class Subscription < ActiveRecord::Base
  belongs_to :plan
  belongs_to :user
  attr_accessible :email, :user_id, :plan_id, :paypal_customer_token, :paypal_payment_token
  validates_presence_of :plan_id, :user_id
  validates_presence_of :email
  # validates_presence_of :paypal_customer_token
  # validates_presence_of :paypal_recurring_profile_token
  
  attr_accessor :paypal_payment_token
  
  def subscription_exists?(current_user)
    Subscription.exists?(user_id: current_user.id)
  end

  def find_subscription(current_user)
    Subscription.find_by_user_id(current_user.id)
  end

  def save_with_payment
    if valid?
      if payment_provided?
        save_with_paypal_payment
      end
    end
  end
  
  def paypal
    PaypalPayment.new(self)
  end
  
  # it seems paypal_customer_token is not the same as paypal recurring profile token
  # paypal_customer_token is called when making payment, 
  # but recurring token is used to store profile_id to make further recurring payment
  def save_with_paypal_payment
    response = paypal.make_recurring
    self.paypal_recurring_profile_token = response.profile_id
    self.status = "active"
    User.status = "paying"
    save!
  end

  def suspend_paypal
  	paypal.suspend
  	self.status = "suspended"
    User.status = "non_paying"
  	save!
  end

  def reactivate_paypal
  	paypal.reactivate
  	self.status = "active"
  	User.status = "paying"
    save!
  end

  def cancel_paypal
  	paypal.cancel
  	self.status = "cancelled"
    User.status = "stopped"
  	save!
  end

  def payment_provided?
    paypal_payment_token.present?
  end
  # def save_with_stripe_payment
  #   customer = Stripe::Customer.create(description: email, plan: plan_id, card: stripe_card_token)
  #   self.stripe_customer_token = customer.id
  #   save!
  # rescue Stripe::InvalidRequestError => e
  #   logger.error "Stripe error while creating customer: #{e.message}"
  #   errors.add :base, "There was a problem with your credit card."
  #   false
  # end
end
