class Player < ActiveRecord::Base
  attr_accessible :name, :number, :pref_position,
  				  :playing_roles, :appearances, :substitutes, :goals,
  				  :assists, :yellow, :red, :traits, 
  				  :tot_points, :team_id, :photo

  belongs_to :team
  has_many :users, through: :lineups
  has_many :matches, through: :squad_players, foreign_key: 'player_id'
  has_many 	 :reviews

  has_attached_file :photo, :default_url => 'images/anonym_player.png',
                            :styles => { :small => "150x150>" },
                            :url  => "/assets/players/:id/:style/:basename.:extension",
                            :path => ":rails_root/public/assets/players/:id/:style/:basename.:extension"

  before_validation :clean_traits

  validates_attachment_size :photo, :less_than => 2.megabytes
  validates_attachment_content_type :photo, :content_type => ['image/jpeg', 'image/jpg', 'image/png']
	validates :name, presence: true
	validates :team_id, presence: true

  # With Squeel gem
	def self.search(query)
		where do
			(name =~ "%#{query}%") | (pref_position =~ "%#{query}%")
		end
	end


	private

  def clean_traits
    self.traits = Sanitize.fragment(self.traits, whitelist)
  end

  def whitelist
    whitelist = Sanitize::Config::RELAXED
    # whitelist[:elements].push("span")
    # whitelist[:attributes]["span"] = ["style"]
    # whitelist
  end



end
