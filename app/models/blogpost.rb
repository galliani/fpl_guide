class Blogpost < ActiveRecord::Base
  attr_accessible :title, :author, :content

  validates :title,   presence: true, length: { maximum: 30 },
  					  uniqueness: { case_sensitive: false }
  validates :author,  presence: true
  validates :content, presence: true, length: { minimum: 30 }

  default_scope order: 'blogposts.created_at DESC'

  before_validation :clean_content

  private

  def clean_content
    self.content = Sanitize.fragment(self.content, whitelist)
  end

  def whitelist
    whitelist = Sanitize::Config::RELAXED
    # whitelist[:elements].push("span")
    # whitelist[:attributes]["span"] = ["style"]
    # whitelist
  end

  # def clean_content
  #   self.content = sanitize_editor(self.content)
  # end

  # def sanitize_editor(field)
  #   ActionController::Base.helpers.sanitize(field,
  #     :tags => %w(a b i strong em p param h1 h2 h3 h4 h5 h6 br hr ul li img),
  #     :attributes => %w(href name src type value width height data) );
  # end


end
