class SquadPlayer < ActiveRecord::Base
  include ActiveModel::Dirty

  attr_accessible :match_id, :player_id, :starting, :lasting, :scored, :assisted,
  				  :cautioned, :sent_off, :rating, :point_gained, :gameweek
  
  belongs_to :match
  belongs_to :player

  validates :player_id, presence: true, uniqueness: true
  validates :match_id, presence: true

  after_update :increment_apps!
  after_update :increment_subs!
  after_update :increment_goals!
  after_update :increment_assists!
  after_update :increment_yellow!
  after_update :increment_red!
  after_update :increment_points!
  # after_update :increment_form!

  # def set_gameweek(match)
  #   self.gameweek = match.gameweek
  # end

  def called?(player)
    Match.squad_players.exists?(player_id: player.id)
  end

  def starter?(match, player)
    Match.squad_players.where(match_id: match.id, player_id: player.id).starting == true
  end

  def subs?(match, player)
    Match.squad_players.where(match_id: match.id, player_id: player.id).starting == false
  end

  def find_player
    Player.find_by_id(player_id)
  end


  private
  # To add stats of players

  def increment_apps! 
    if self.starting != self.starting_was
      if self.starting == true
        find_player.increment!(:appearances)
      else
        find_player.decrement!(:appearances)
      end
    end
  end

  def increment_subs! 
    if self.lasting != self.lasting_was
      if self.starting == false && self.lasting > 0
        find_player.increment!(:substitutes)
      else
        find_player.decrement!(:substitutes)
      end
    end
  end

  def increment_goals!
    if self.scored != self.scored_was
      if self.scored < self.scored_was
        find_player.decrement!(:goals, self.scored_was - self.scored)
      else
        find_player.increment!(:goals, self.scored - self.scored_was)
      end
    end
  end

  def increment_assists!
    if self.assisted != self.assisted_was
      if self.assisted < self.assisted_was
        find_player.decrement!(:assists, self.assisted_was - self.assisted)
      else
        find_player.increment!(:assists, self.assisted - self.assisted_was)
      end
    end
  end  

  def increment_yellow!
    if self.cautioned != self.cautioned_was
      if self.cautioned == true
        find_player.increment!(:yellow)
      else 
        find_player.decrement!(:yellow)
      end
    end
  end  

  def increment_red!
    if self.sent_off != self.sent_off_was
      if self.sent_off == true
        find_player.increment!(:red)
      else
        find_player.decrement!(:red)
      end
    end
  end

  def increment_points!
    if self.point_gained != self.point_gained_was
      if self.point_gained < self.point_gained_was
        find_player.decrement!(:tot_point, self.point_gained_was - self.point_gained)
      else
        find_player.increment!(:tot_point, self.point_gained - self.point_gained_was)
      end
    end
  end

  def increment_form!
    if self.rating != self.rating_was
      find_player.form_record[0...-2].concat(self.rating.to_s)
    else
      find_player.form_record.concat(self.rating.to_s)
    end
  end

end
