class Plan < ActiveRecord::Base
  attr_accessible :description, :duration, :name, :price
  has_many :subscriptions

  validates :name, presence: true
  validates :price, presence: true
  validates :duration, presence: true
end
