class TeamStat < ActiveRecord::Base
	include ActiveModel::Dirty

	attr_accessible :point, :score, :concede, :shot, :on_target, :offside, :foul,
					:yellow_card, :red_card, :possession, :team_id
	belongs_to :match
	belongs_to :team
	belongs_to :hometeam, class_name: 'Team', foreign_key: 'team_id'
	belongs_to :awayteam, class_name: 'Team', foreign_key: 'team_id'

  validates :match_id, presence: true
  validates :team_id, presence: true
  validates_numericality_of :point, allow_nil: false, :only_integer => true
  validates_numericality_of :score, allow_nil: false, :only_integer => true
  validates_numericality_of :concede, allow_nil: false, :only_integer => true
  validates_numericality_of :shot, :only_integer => true
  validates_numericality_of :on_target, :only_integer => true
  validates_numericality_of :offside, :only_integer => true
  validates_numericality_of :yellow_card, :only_integer => true
  validates_numericality_of :red_card, :only_integer => true

  after_save :determine_points!
  after_save :increment_league_points!
  after_save :increment_goals_for!
  after_save :increment_goals_against!
  after_save :increment_attempts!
  after_save :increment_on_targets!
  after_save :increment_offsides!
  # after_save :increment_fouls! no column, use attr_writer instead
  after_save :increment_yellows!
  after_save :increment_reds!
  after_save :calc_gd!
  # after_save :update_standings!

  def find_team
    Team.find_by_id(team_id)
  end

  def sort_teams
    Team.order('league_points DESC, goal_difference DESC')
  end

  private # To add stats of teams

  def update_standings! #Bugged, still not working
    find_team.league_position = sort_teams.to_a.map{ |team| team.ticker }.index(find_team.ticker) + 1
  end

  def determine_points!
  	if self.point_was == nil 
		 	if self.point == 3
        find_team.increment!(:won, 1)
		  elsif self.point = 1
        find_team.increment!(:drawn, 1) 
		  elsif self.point = 0
        find_team.increment!(:lost, 1)
		  end
		elsif self.point_was != self.point
		 	if self.point_was == 1 && self.point == 3
          find_team.increment!(:won, 1)
          find_team.decrement!(:drawn, 1)
      elsif self.point_was == 1 && self.point == 0
        find_team.increment!(:lost, 1)
        find_team.decrement!(:drawn, 1)
      elsif self.point_was == 0 && self.point == 3
          find_team.increment!(:won, 1)
          find_team.decrement!(:lost, 1)
		  elsif self.point_was == 0 && self.point == 1
          find_team.increment!(:drawn, 1)
          find_team.decrement!(:lost, 1)      
      elsif self.point_was == 3 && self.point == 1
          find_team.increment!(:drawn, 1)
          find_team.decrement!(:won, 1)        
		  elsif self.point_was == 3 && self.point == 0
          find_team.increment!(:lost, 1)
          find_team.decrement!(:won, 1)
		  end			
  	end
  end

  def increment_league_points!
    if self.point_was == nil
      find_team.increment!(:league_points, self.point)
    else
    	if self.point != self.point_was
    		if self.point < self.point_was
    			find_team.decrement!(:league_points, self.point_was - self.point)
    		else
    			find_team.increment!(:league_points, self.point - self.point_was)
    		end
    	end
    end
  end

  def increment_goals_for!
    if self.score_was == nil
      find_team.increment!(:goals_for, self.score)
    else
    	if self.score != self.score_was
    		if self.score < self.score_was
    			find_team.decrement!(:goals_for, self.score_was - self.score)
    		else
    			find_team.increment!(:goals_for, self.score - self.score_was)
    		end
    	end
    end
  end

  def increment_goals_against!
    if self.score_was == nil
      find_team.increment!(:goals_against, self.concede)
    else
    	if self.concede != self.concede_was
    		if self.concede < self.concede_was
    			find_team.decrement!(:goals_against, self.concede_was - self.concede)
    		else
    			find_team.increment!(:goals_against, self.concede - self.concede_was)
    		end
    	end
    end
  end

  def calc_gd!
    gd = self.score - self.concede
    if self.score_was == nil || self.concede_was == nil
      old_gd = 0
    elsif self.score_was != nil || self.concede_was != nil
      old_gd = self.score_was - self.concede_was
    end

    if self.score_was == nil && self.concede_was == nil
      find_team.increment!(:goal_difference, gd)
    else
      if gd < old_gd
        find_team.decrement!(:goal_difference, old_gd - gd)
      elsif gd > old_gd
        find_team.increment!(:goal_difference, gd - old_gd)
      end
    end
  end

  def increment_attempts!
    if self.shot_was == nil
      find_team.increment!(:attempts, self.shot)
    else
    	if self.shot != self.shot_was
    		if self.shot < self.shot_was
    			find_team.decrement!(:attempts, self.shot_was - self.shot)
    		else
    			find_team.increment!(:attempts, self.shot - self.shot_was)
    		end
    	end
    end
  end

  def increment_on_targets!
    if self.on_target_was == nil
      find_team.increment!(:on_targets, self.on_target)
    else    
    	if self.on_target != self.on_target_was
    		if self.on_target < self.on_target_was
    			find_team.decrement!(:on_targets, self.on_target_was - self.on_target)
    		else
    			find_team.increment!(:on_targets, self.on_target - self.on_target_was)
    		end
    	end
    end
  end

  def increment_offsides!
    if self.offside_was == nil
      find_team.increment!(:offsides, self.offside)
    else
    	if self.offside != self.offside_was
    		if self.offside < self.offside_was
    			find_team.decrement!(:offsides, self.offside_was - self.offside)
    		else
    			find_team.increment!(:offsides, self.offside - self.on_target_was)
    		end
    	end
    end
  end

  def increment_fouls!
    if self.foul_was == nil
      find_team.increment!(:fouls, self.foul)
    else
    	if self.foul != self.foul_was
    		if self.foul < self.foul_was
    			find_team.decrement!(:fouls, self.foul_was - self.foul)
    		else
    			find_team.increment!(:fouls, self.foul - self.foul_was)
    		end
    	end
    end
  end

  def increment_yellows!
    if self.yellow_card_was == nil
      find_team.increment!(:yellows, self.yellow_card_was)
    else    
    	if self.yellow_card != self.yellow_card_was
    		if self.yellow_card < self.yellow_card_was
    			find_team.decrement!(:yellows, self.yellow_card_was - self.yellow_card)
    		else
    			find_team.increment!(:yellows, self.yellow_card - self.yellow_card_was)
    		end
    	end
    end
  end

  def increment_reds!
    if self.red_card_was == nil
      find_team.increment!(:reds, self.red_card)
    else    
    	if self.red_card != self.red_card_was
    		if self.red_card < self.red_card_was
    			find_team.decrement!(:reds, self.red_card_was - self.red_card)
    		else
    			find_team.increment!(:reds, self.red_card - self.red_card_was)
    		end
    	end
    end
  end

  def increment_possessions!
    if self.possession_was == nil
      find_team.increment!(:possessions, self.possession)
    else
    	if self.possession != self.possession_was
    		if self.possession < self.possession_was
    			find_team.decrement!(:possessions, self.possession_was - self.possession)
    		else
    			find_team.increment!(:possessions, self.possession - self.possession_was)
    		end
    	else
  			find_team.increment!(:possessions, self.possession)
    	end
    end
  end

end