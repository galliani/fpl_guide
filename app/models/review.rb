class Review < ActiveRecord::Base
  include ActiveModel::Dirty

  belongs_to :player
  attr_accessible :content, :title, :rating, :match_id

  validates :title, presence: true, :on=>:create
  validates :rating, presence: true, length: { maximum: 2 }
  validates :content, presence: true, :on=>:create

  before_validation :clean_content
  # after_save :latest_form
    
  # def latest_form!
  #   player_form

  #   if self.rating != self.rating_was
  #     player.player_form.pop
  #     player.player_form.push(self.rating)
  #   else
  #     player.player_form.push(self.rating)
  #   end

  #   recent = player.player_form.last(5)
  #   find_player.recent_form = recent.sum.to_f / recent.count
  # end

  private

  def clean_content
    self.content = Sanitize.fragment(self.content, whitelist)
  end

  def whitelist
    whitelist = Sanitize::Config::RELAXED
    # whitelist[:elements].push("span")
    # whitelist[:attributes]["span"] = ["style"]
    # whitelist
  end
end
