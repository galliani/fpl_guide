class PaypalPayment
  def initialize(subscription)
    @subscription = subscription
  end
  
  def checkout_details
    process :checkout_details
  end
  
  def checkout_url(options)
    process(:checkout, options).checkout_url
  end
  
  def make_recurring
    process :request_payment
    process :create_recurring_profile, period: :monthly, 
                                       frequency: @subscription.plan.duration, 
                                       start_at: Date.current + 14.days, 
                                       failed: 1,
                                       outstanding: :next_billing,
                                       trial_length: 14,
                                       trial_period: :daily,
                                       trial_frequency: 1
  end

  def suspend
    process :suspend, :profile_id => @subscription.paypal_recurring_profile_token
  end

  def reactivate
    process :reactivate, :profile_id => @subscription.paypal_recurring_profile_token
  end

  def cancel
    process :cancel, :profile_id => @subscription.paypal_recurring_profile_token
  end


  private

  def process(action, options = {})
    options = options.reverse_merge(
      payer_id: @subscription.paypal_customer_token,
      token: @subscription.paypal_payment_token,
      description: @subscription.plan.name,
      amount: @subscription.plan.price,
      currency: "USD"
    )
    response = PayPal::Recurring.new(options).send(action)
    raise response.errors.inspect if response.errors.present?
    response
  end
end