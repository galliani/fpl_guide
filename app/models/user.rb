class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :username, :email, :password, :password_confirmation, 
                  :remember_me, :fantasy_team_name, :photo

  has_attached_file :photo, :default_url => 'images/anonym_user.jpg',
                            :styles => { :small => "150x150>" },
                            :url  => "/assets/users/:id/:style/:basename.:extension",
                            :path => ":rails_root/public/assets/users/:id/:style/:basename.:extension"

  has_one :subcription
  has_many :lineups
  has_many :players, through: :lineups

  validates_attachment_size :photo, :less_than => 2.megabytes
  validates_attachment_content_type :photo, :content_type => ['image/jpeg', 'image/jpg', 'image/png']
  validates_presence_of     :username
  validates_uniqueness_of   :username, :case_sensitive => false 
  validates_length_of       :username, maximum: 25, minimum: 5 

  # validates_presence_of     :status
  validates_presence_of     :fantasy_team_name
  
  # Devise Validatable
  validates_presence_of     :email
  validates_uniqueness_of   :email,    :case_sensitive => false, :allow_blank => true, :if => :email_changed?
  validates_format_of       :email,    :with  => Devise.email_regexp, :allow_blank => true, :if => :email_changed?
  
  validates_presence_of     :password, :on=>:create
  validates_confirmation_of :password, :on=>:create
  validates_length_of       :password, :on=>:create, :within => Devise.password_length

  before_update :check_password
  
  def subscribed?
    subscription.present?
  end

  def picked?(player)
    self.lineups.find_by_player_id(player.id)
  end

  def pick!(player)
    self.lineups.create!(player_id: player.id)
  end

  def unpick!(player)
    self.lineups.find_by_player_id(player.id).destroy
  end

  def fantasyteam
    self.lineups.where(user_id: self.id)
  end
  
  private

    def check_password
      is_ok = self.password.nil? || self.password.empty? || self.password.length >= 8
      self.errors[:password] << "Password is too short (minimum is 8 characters)" unless is_ok
      is_ok # The callback returns a Boolean value indicating success; if it fails, the save is blocked
    end



end
