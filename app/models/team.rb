class Team < ActiveRecord::Base
  include ActiveModel::Dirty
  attr_accessible :name, :ticker, :league_position, :league_points, :goals_for, 
  				  :goals_against, :attempts, :on_targets, :offsides, :yellows, 
            :reds, :possession,  :playing_style, :photo,
            :won, :drawn, :lost, :goal_difference

  has_many :players, dependent: :destroy
  has_many :home_matches, class_name: 'Match', foreign_key: 'hometeam_id'
  has_many :away_matches, class_name: 'Match', foreign_key: 'awayteam_id' 
  has_many :team_stats
  has_attached_file :photo, :default_url => ":rails_root/public/assets/anonym_team.jpg",
                            :styles => { :small => "150x150>", :bg => "400x300>" },
                            :url  => "/assets/teams/:id/:style/:basename.:extension",
                            :path => ":rails_root/public/assets/teams/:id/:style/:basename.:extension"

  before_save { |team| team.name = team.name.upcase }
  before_validation :clean_playing_style
  validates :name, presence: true, 
  				         uniqueness: { case_sensitive: false }
  validates :ticker, presence: true
  validates_attachment_size :photo, :less_than => 2.megabytes
  validates_attachment_content_type :photo, :content_type => ['image/jpeg', 'image/jpg', 'image/png']
  validates :goals_for, presence: true, on: :update
  validates :goals_against, presence: true, on: :update
  validates :playing_style, length: { maximum: 150 }
  

  private

  def clean_playing_style
    self.playing_style = Sanitize.fragment(self.playing_style, whitelist)
  end

  def whitelist
    whitelist = Sanitize::Config::RELAXED
    # whitelist[:elements].push("span")
    # whitelist[:attributes]["span"] = ["style"]
    # whitelist
  end
  # def check_league_form!
  #   if self.point != self.point_was
  #     if self.point == 3
  #       find_team.league_form.sub!(/[WDL]$/, 'W')
  #     elsif self.point == 1
  #       find_team.league_form.sub!(/[WDL]$/, 'D')
  #     elsif self.point == 0
  #       find_team.league_form.sub!(/[WDL]$/, 'L')
  #     end
  #   end
  # end
end
