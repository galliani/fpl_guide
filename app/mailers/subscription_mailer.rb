class SubscriptionMailer < ActionMailer::Base
  default from: "info@fplguide.com"

  def welcome(subscriber)
    @subscriber = subscriber
    mail(to: subscriber.email, subject: "Welcome to the club....")
  end
end