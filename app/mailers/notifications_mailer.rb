class NotificationsMailer < ActionMailer::Base
  default from: "info@fplguide.com"
  default to: "galih0muhammad@gmail.com"

  def new_message(message)
    @message = message
    mail(:subject => "[FPLGuide.com] #{message.subject}")
  end

end
