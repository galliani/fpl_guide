module MatchesHelper

  # def called?(player)
  #   self.squad_players.exists?(player_id: player.id)
  # end

  def called?(player)
    SquadPlayer.exists?(player_id: player.id)
  end
  
  def find_squadplayer(match, player)
  	SquadPlayer.find_by_match_id_and_player_id(match.id, player.id)
  end 

  def starter?(match, player)
    Match.squad_players.where(match_id: match.id, player_id: player.id).starting = true
  end

  def find_result(match, team)
    TeamStat.find_by_match_id_and_team_id(match.id, team.id)
  end
end