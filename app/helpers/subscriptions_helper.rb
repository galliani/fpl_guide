module SubscriptionsHelper

  def subscription_exists?(current_user)
    Subscription.exists?(user_id: current_user.id)
  end

  def find_subscription(current_user)
    Subscription.find_by_user_id(current_user.id)
  end
end
