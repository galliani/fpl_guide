module ApplicationHelper

	def full_title(page_title)
	  base_title = "FPL Guide"
	  if page_title.empty?
	    base_title
	  else
	    "#{base_title} | #{page_title}"
	  end
	end

 	def bootstrap_class_for flash_key
    case flash_key
      when :success
        "alert-success"
      when :error
        "alert-error"
      when :alert
        "alert-block"
      when :notice
        "alert-info"
      else
        flash_key.to_s
    end
  end

 def alert_class_for(flash_key)
    {
      :success => 'alert-success',
      :error => 'alert-danger',
      :alert => 'alert-warning',
      :notice => 'alert-info'
    }[flash_key.to_sym] || flash_key.to_s
  end

  def set_navbar
    if params[:controller] == "subscribers" && params[:action] == "landing_page"
      render layout: false
    else
      render partial: 'layouts/header'
    end
  end


  # Creating an admin on the console
  # current_user.update_attribute :admin, true
  def avatar_for(player, options = { size: 52 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    avatar_url = player.image
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end



end
