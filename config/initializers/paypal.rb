require "paypal/recurring"

PayPal::Recurring.configure do |config|
  # if Rails.env.development? || Rails.env.test?
  # 	config.sandbox = true
  # else
  # 	config.sandbox = false
  # end
  
  config.sandbox = true
  config.username = ENV["PAYPAL_SANDBOX_LOGIN"]
  config.password = ENV["PAYPAL_SANDBOX_PASS"]
  config.signature = ENV["PAYPAL_SANDBOX_SIG"]
end