FplGuide::Application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'  
  # Payment
  # resources :subscriptions
  # resources :plans, skip: [:show]
  # get 'paypal/checkout', to: 'subscriptions#paypal_checkout'
  # get 'paypal/suspend', to: 'subscriptions#paypal_suspend'
  # get 'paypal/reactivate', to: 'subscriptions#paypal_reactivate'
  # get 'paypal/cancel', to: 'subscriptions#paypal_cancel'

  resources :blogposts

  # Guides
  resources :teams
  resources :players do
    resources :reviews, skip: [:index, :show]
  end  
  resources :reviews, only: [:destroy]
  resources :matches do
    match 'squad_players/all/edit' => 'squad_players#edit_all', :as => :edit_squad_players, :via => :get
    match 'squad_players/all' => 'squad_players#update_all', :as => :update_squad_players, :via => :put 
    resources :squad_players, only: [:edit, :update, :create, :destroy]
    match 'team_stats/all/edit' => 'team_stats#edit_all', :as => :edit_team_stats, :via => :get 
    match 'team_stats/all' => 'team_stats#update_all', :as => :update_team_stats, :via => :put 
    resources :team_stats, skip: [:index, :show]
  end
  resources :lineups, only: [:create, :destroy]
  
  # Users
  devise_for :users, skip: [:confirmations, :unlock]

  # Static Pages
  match 'home',              to: 'static_pages#home', as: :user_root, :via => :get
  match 'help',              to: 'static_pages#help'
  match 'under_development', to: 'static_pages#under_development'  
  match 'dashboard',         to: 'static_pages#dashboard'
  match 'landing',           to: 'subscribers#landing_page'
  root                       to: 'subscribers#landing_page'

  resources :users, only: [:show]
  
  # Mailer

  resources :subscribers
  match 'contact' => 'contact#create', :as => 'contact', :via => :post

  if Rails.env.development?
    mount MailPreview => 'mail_view'
  end




  # match "/orders/express", to: 'orders#express'

  # resources :orders, only: [:new, :create]

  # Default routes from Devise, uncomment it if neccessary
  # devise_for :users

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
