require 'rails_helper'

RSpec.describe Plan, :type => :model do
  let!(:plan) { FactoryGirl.create(:plan) }

  subject { plan }

  it { should be_valid }

  it { should respond_to(:description) }
  it { should respond_to(:duration) }
  it { should respond_to(:name) }
  it { should respond_to(:price) }
  it { should respond_to(:subscriptions) }

  describe "when plan name not present" do
  	before { plan.name = " " }
  	it { should_not be_valid }
  end

  describe "when price not present" do
  	before { plan.price = nil }
  	it { should_not be_valid }
  end

  describe "when duration not present" do
  	before { plan.duration = nil }
  	it { should_not be_valid }
  end
end
