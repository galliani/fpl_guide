require 'rails_helper'

RSpec.describe User, :type => :model do
  before do
    @user = User.new(username: "example", email: "user@example.com",
  							     password: "foobarbaz", password_confirmation: "foobarbaz",
                     fantasy_team_name: "Team_1")
  end

  subject { @user }

  it { should respond_to(:username) }
  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:status) }
  it { should respond_to(:remember_me) }
  it { should respond_to(:admin) }
  it { should respond_to(:lineups) }
  it { should respond_to(:players) }

  it { should_not be_admin }
  it { should be_valid }

  describe "accessible attributes:" do
    it "should not allow access to admin" do
      expect do
        User.new(admin: "1")
      end.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end

    it "should not allow access to status" do
      expect do
        User.new(status: "paying")
      end.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end    
  end

  describe "when username is not present" do
  	before { @user.username = " " }
  	it { should_not be_valid }
  end

  describe "when email is not present" do
  	before { @user.email = " " }
  	it { should_not be_valid }
  end

  describe "when password is not present" do
  	before { @user.password = " "}
  	it { should_not be_valid }
  end

  describe "when status is not present" do
    before { @user.status = " "}
    it { should_not be_valid }
  end

  describe "when password confirmation is not present" do
  	before { @user.password_confirmation = " "}
  	it { should_not be_valid }
  end

  describe "when username is too long" do
    before { @user.username = "a" * 26}
    it { should_not be_valid }
  end

  describe "when username is too short" do
    before { @user.username = "a" * 4}
    it { should_not be_valid }
  end

  describe "email format is not valid" do
    it "should not be valid" do
      addresses = %w[ user@foo,com user_at_foo.org example_user@foo. ]
      addresses.each do |invalid_address|
        @user.email = invalid_address
        @user.should_not be_valid
      end
    end
  end

  describe "email format is valid" do
    it "should be valid" do
      addresses = %w[ user@foo.COM A_US-ER@f.b.org first.list@foo.jp a+b@foo.id ]
      addresses.each do |valid_address|
        @user.email = valid_address
        @user.should be_valid
      end
    end
  end

  describe "when email address is already taken" do
    before do
      user_with_same_email = @user.dup
      user_with_same_email.email = @user.email.upcase
      user_with_same_email.save
    end

    it { should_not be_valid }
  end

  describe "when password is in blank" do
    before { @user.password = @user.password_confirmation = " " }
    it { should_not be_valid }
  end

  describe "when password is too short" do
    before { @user.password = @user.password_confirmation = "a" * 7 }
    it { should_not be_valid }
  end

  describe "when password does not match confirmation" do
    before { @user.password_confirmation = "mismatch" }
    it { should_not be_valid }
  end
end
