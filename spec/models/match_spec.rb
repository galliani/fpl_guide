require 'rails_helper'

RSpec.describe Match, :type => :model do
	let!(:team1) { FactoryGirl.create(:team) }
	let!(:team2) { FactoryGirl.create(:team) }

	before do
		@match = Match.new(hometeam_id: team1.id, awayteam_id: team2.id,
											 gameweek: 1, kickoff: "2014-10-24 15:15:00",
											 prematch: "Lorem Ipsum Dolor",
											 postmatch: "Lorem Ipsum Dolor",)
	end

	subject { @match }

	it { should respond_to(:hometeam) }
	it { should respond_to(:hometeam_id) }
	it { should respond_to(:awayteam) }
	it { should respond_to(:awayteam_id) }
	it { should respond_to(:gameweek) }
	it { should respond_to(:kickoff) }
	it { should respond_to(:prematch) }
	it { should respond_to(:postmatch) }

	it { should be_valid }

	describe "when home team is not present" do
		before { @match.hometeam_id = " " }
		it { should_not be_valid }
	end

	describe "when away team is not present" do
		before { @match.awayteam_id = " " }
		it { should_not be_valid }
	end

	describe "when gameweek is not present" do
		before { @match.gameweek = " " }
		it { should_not be_valid }
	end

end
