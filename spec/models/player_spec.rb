require 'rails_helper'

RSpec.describe Player, :type => :model do

	let(:team) { FactoryGirl.create(:team) }
	before do
		@player = team.players.build(name: "Michael", number: 10,
												 pref_position: "Winger", 
												 playing_roles: "Playmaker",
												 appearances: 7, goals: 2,
												 assists: 5, yellow: 3, red: 0,
												 traits: "Creative, Passer")
	end

	subject { @player }

	it { should respond_to(:name) }
	it { should respond_to(:number) }
	it { should respond_to(:pref_position) }
	it { should respond_to(:playing_roles) }
	it { should respond_to(:appearances) }
	it { should respond_to(:substitutes) }
	it { should respond_to(:goals) }
	it { should respond_to(:assists) }
	it { should respond_to(:yellow) }
	it { should respond_to(:red) }
	it { should respond_to(:traits) }
	it { should respond_to(:tot_points) }
	it { should respond_to(:photo) }
	it { should respond_to(:team_id) }
	it { should respond_to(:team) }
	it { should respond_to(:reviews) }
	it { should respond_to(:users) }
	it { should respond_to(:matches) }

	it { should be_valid }

	describe "when name is not present" do
		before { @player.name = " " }
		it { should_not be_valid }
	end

	# describe "when number is not present" do
	# 	before { @player.number = " " }
	# 	it { should_not be_valid }
	# end

	# describe "when pref_position is not present" do
	# 	before { @player.pref_position = " " }
	# 	it { should_not be_valid }
	# end

	# describe "when playing_roles is not present" do
	# 	before { @player.playing_roles = " " }
	# 	it { should_not be_valid }
	# end

	# describe "when player's traits is not present" do
	# 	before { @player.traits = " " }
	# 	it { should_not be_valid }
	# end

	describe "when player's team_id is not present" do
		before { @player.team_id = " " }
		it { should_not be_valid }
	end

	describe "when the length of player number is too long" do
		before { @player.number = "1" * 3 }
		it { should_not be_valid }
	end

	describe "when the length of number of appearances is too long" do
		before { @player.appearances = "1" * 3 }
		it { should_not be_valid }
	end

	describe "when the length of player goals is too long" do
		before { @player.goals = "1" * 3 }
		it { should_not be_valid }
	end

	describe "when the length of player assists is too long" do
		before { @player.assists = "1" * 3 }
		it { should_not be_valid }
	end

	describe "when the length of player's yellow card booking is too long" do
		before { @player.yellow = "1" * 3 }
		it { should_not be_valid }
	end

	describe "when the length of player's red card booking is too long" do
		before { @player.red = "1" * 3 }
		it { should_not be_valid }
	end


end
