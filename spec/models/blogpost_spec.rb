require 'rails_helper'

RSpec.describe Blogpost, :type => :model do

	before do
		@blogpost = Blogpost.new(title: "Blogpost Numero Uno", author: "Michael",
														 content: "Lorem ipsum dolor lorem ipsum
														 					 Lorem ipsum dolor lorem ipsum
														 					 Lorem ipsum dolor lorem ipsum
														 					 Lorem ipsum dolor lorem ipsum
														 					 Lorem ipsum dolor lorem ipsum")
	end

  subject { @blogpost }

  it { should respond_to(:title) }
  it { should respond_to(:author) }
  it { should respond_to(:content) }

  it { should be_valid }

  describe "when title is not present" do
  	before { @blogpost.title = " " }
  	it { should_not be_valid }
  end

  describe "when author is not present" do
  	before { @blogpost.author = " " }
  	it { should_not be_valid }
  end

  describe "when content is not present" do
  	before { @blogpost.content = " " }
  	it { should_not be_valid }
  end

  describe "when title is too long" do
  	before { @blogpost.title = "a" * 31 }
  	it { should_not be_valid }
  end

  describe "when content is too short" do
  	before { @blogpost.content = "a" * 29 }
  	it { should_not be_valid }
  end

  describe "whem there is a duplication in title" do
  	before do
  		blogpost_with_duplication = @blogpost.dup
  		blogpost_with_duplication.title = @blogpost.title.upcase
  		blogpost_with_duplication.save
  	end

  	it { should_not be_valid }
  end

end
