require 'rails_helper'

RSpec.describe Lineup, :type => :model do
	let!(:user) { FactoryGirl.create(:paying_user) }
	let!(:player1) { FactoryGirl.create(:player) }
	let!(:lineup) { user.lineups.build(player_id: player1.id) }

	subject { lineup }

	it { should be_valid }
	it { should respond_to(:role) }
	it { should respond_to(:player) }
	it { should respond_to(:user) }

  describe "accessible attributes" do
    it "should not allow access to user id" do
      expect do
        Lineup.new(user_id: user.id)
      end.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end

  describe "when user id not present" do
  	before { lineup.user_id = nil }
  	it { should_not be_valid }
  end

  describe "when player id not present" do
  	before { lineup.player_id = nil }
  	it { should_not be_valid }
  end
end
