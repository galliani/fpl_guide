require 'rails_helper'

RSpec.describe SquadPlayer, :type => :model do
	let!(:team1) { FactoryGirl.create(:team) }
	let!(:team2) { FactoryGirl.create(:team) }
	let!(:match) { FactoryGirl.create(:match, hometeam_id: team1.id, 
											  awayteam_id: team2.id, 
											  gameweek: 1) }
	let!(:player) { FactoryGirl.create(:player) }
	let!(:squad_player) { match.squad_players.build(player_id: player.id) }

	subject { squad_player }

	it { should be_valid }
	it { should respond_to(:match_id) }
	it { should respond_to(:player_id) }
	it { should respond_to(:match) }
	it { should respond_to(:player) }
	it { should respond_to(:starting) }
	it { should respond_to(:lasting) }
	it { should respond_to(:scored) }
	it { should respond_to(:assisted) }
	it { should respond_to(:cautioned) }
	it { should respond_to(:sent_off) }
	it { should respond_to(:rating) }
	it { should respond_to(:point_gained) }
	it { should respond_to(:gameweek) }
	it { should respond_to(:called?) }
	it { should respond_to(:starter?) }
	it { should respond_to(:subs?) }

  describe "when player id not present" do
  	before { squad_player.player_id = nil }
  	it { should_not be_valid }
  end

  describe "when match id not present" do
  	before { squad_player.match_id = nil }
  	it { should_not be_valid }
  end

end
