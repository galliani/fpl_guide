require 'rails_helper'

RSpec.describe SquadPlayer, :type => :model do
	let!(:team1) { FactoryGirl.create(:team) }
	let!(:team2) { FactoryGirl.create(:team) }
	let!(:match) { FactoryGirl.create(:match, hometeam_id: team1.id, 
											  awayteam_id: team2.id, 
											  gameweek: 1) }
	let!(:team_stat) { FactoryGirl.create(:team_stat, team: match.hometeam,
																				match: match, point: 3, score: 1, concede: 0) }
	
	subject { team_stat }

	it { should respond_to(:match_id) }
	it { should respond_to(:team_id) }
	it { should respond_to(:match) }
	it { should respond_to(:team) }
	it { should respond_to(:point) }
	it { should respond_to(:score) }
	it { should respond_to(:concede) }
	it { should respond_to(:shot) }
	it { should respond_to(:on_target) }
	it { should respond_to(:offside) }
	it { should respond_to(:foul) }
	it { should respond_to(:yellow_card) }
	it { should respond_to(:red_card) }


	it { should be_valid }

  describe "when match id not present" do
  	before { team_stat.match_id = nil }
  	it { should_not be_valid }
  end

  describe "when team id not present" do
  	before { team_stat.team_id = nil }
  	it { should_not be_valid }
  end

  describe "when point is not integer" do
  	it "should not be valid" do
	  	invalid_data = /\D/
	  	team_stat.point = invalid_data
	  	team_stat.should_not be_valid
	  end
  end

  describe "when score is not integer" do
  	it "should not be valid" do
	  	invalid_data = /\D/
	  	team_stat.score = invalid_data
	  	team_stat.should_not be_valid
	  end
  end
  describe "when concede is not integer" do
  	it "should not be valid" do
	  	invalid_data = /\D/
	  	team_stat.concede = invalid_data
	  	team_stat.should_not be_valid
	  end
  end

  describe "when shot is not integer" do
  	it "should not be valid" do
	  	invalid_data = /\D/
	  	team_stat.shot = invalid_data
	  	team_stat.should_not be_valid
	  end
  end

  describe "when shot on target is not integer" do
  	it "should not be valid" do
	  	invalid_data = /\D/
	  	team_stat.on_target = invalid_data
	  	team_stat.should_not be_valid
	  end
  end

  describe "when offside is not integer" do
  	it "should not be valid" do
	  	invalid_data = /\D/
	  	team_stat.offside = invalid_data
	  	team_stat.should_not be_valid
	  end
  end

  describe "when yellow card number is not integer" do
  	it "should not be valid" do
	  	invalid_data = /\D/
	  	team_stat.yellow_card = invalid_data
	  	team_stat.should_not be_valid
	  end
  end

  describe "when red card number is not integer" do
  	it "should not be valid" do
	  	invalid_data = /\D/
	  	team_stat.red_card = invalid_data
	  	team_stat.should_not be_valid
	  end
  end

end