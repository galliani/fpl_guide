require 'rails_helper'

RSpec.describe Review, :type => :model do

	let(:player) { FactoryGirl.create(:player) }

	before do
		@review = player.reviews.build(title: "Good performance",
																	 rating: 8, 
																	 content: "Bossing the midfield, 
																	 distributing passes excellently")
	end

	subject { @review }

	it { should respond_to(:title) }
	it { should respond_to(:rating) }
	it { should respond_to(:content) }
	it { should respond_to(:player_id) }
	it { should respond_to(:player) }

	it { should be_valid }

	describe "when title is blank" do
		before { @review.title = " " }
		it { should_not be_valid }
	end

	describe "when rating is blank" do
		before { @review.rating = " " }
		it { should_not be_valid }
	end

	describe "when content is blank" do
		before { @review.content = " " }
		it { should_not be_valid }
	end
end
