require 'rails_helper'

RSpec.describe Team, :type => :model do
	before do
		@team = Team.new(name: "FC Example", ticker: "FCE",
						 league_position: "5", 
						 goals_for: "51", goals_against: "20",
						 playing_style: "good on counterattack")
	end

	subject { @team }

	it { should respond_to(:name) }
	it { should respond_to(:league_position) }
	it { should respond_to(:goals_for) }
	it { should respond_to(:goals_against) }
	it { should respond_to(:goal_difference) }
	it { should respond_to(:yellows) }
	it { should respond_to(:reds) }
	it { should respond_to(:attempts) }
	it { should respond_to(:on_targets) }
	it { should respond_to(:offsides) }
	it { should respond_to(:playing_style) }
	it { should respond_to(:photo) }
	it { should respond_to(:players) }
	it { should respond_to(:team_stats) }
	it { should respond_to(:home_matches) }
	it { should respond_to(:away_matches) }

	it { should be_valid }

	describe "when name is not present" do
		before { @team.name = " " }
		it { should_not be_valid }
	end

	# describe "when goals_for is not present" do
	# 	before { @team.goals_for = " " }
	# 	it { should_not be_valid }
	# end

	# describe "when goals_against is not present" do
	# 	before { @team.goals_against = " " }
	# 	it { should_not be_valid }
	# end

	# describe "when league_form is not present" do
	# 	before { @team.league_form = " " }
	# 	it { should_not be_valid }
	# end

	# describe "when next_opps is not present" do
	# 	before { @team.next_opps = " " }
	# 	it { should_not be_valid }
	# end

	# describe "when playing_style is not present" do
	# 	before { @team.playing_style = " " }
	# 	it { should_not be_valid }
	# end

	describe "when the length of playing_style is too long" do
		before { @team.playing_style = "A" * 160 }
		it { should_not be_valid }
	end

	describe "when there is a duplication in team name" do
		before do
			team_with_duplication = @team.dup
			team_with_duplication.name = @team.name.upcase
			team_with_duplication.save
		end

		it { should_not be_valid }
	end

end
