def full_title(page_title)
  base_title = "FPL Guide"
  if page_title.empty?
    base_title
  else
    "#{base_title} | #{page_title}"
  end
end

def post(review)
  fill_in("review[title]", with: review.title, :match => :prefer_exact)
  fill_in("review[rating]", with: review.rating, :match => :prefer_exact)
  fill_in("review[content]", with: review.content, :match => :prefer_exact)
  click_button "Post Review"
end 

def sign_in(user)
	visit new_user_session_path
	fill_in("user[email]", with: user.email, :match => :prefer_exact)
	fill_in("user[password]", with: user.password, :match => :prefer_exact)
	click_button  "Sign In"
end

def sign_out
  first(:link, "Sign Out").click
end

