require 'rails_helper'

RSpec.describe "UserPages", :type => :request do

	subject { page }

	describe "signup page" do
		before { visit new_user_registration_path }

		it { should have_content('Sign Up') }
		it { should have_title('Sign Up') }
	end

	describe "signup" do	
		before { visit new_user_registration_path }

		let(:submit) { "Sign Up" }

		describe "with invalid information" do
			it "should not create a user" do
				expect { click_button submit }.not_to change(User, :count)
			end

			describe "after submission" do
				before { click_button submit }
				it { should have_title('Sign Up') }
				it { should have_selector('div.alert') }
			end
		end

		describe "with valid information" do
			before do
				fill_in("user[username]", with: "exampleuser", :match => :prefer_exact)
				fill_in("user[email]", with: "user@Example.com", :match => :prefer_exact)
				fill_in("user[password]", with: "foobarbaz", :match => :prefer_exact)
				fill_in("user[password_confirmation]", with: "foobarbaz", :match => :prefer_exact)
				fill_in("user[fantasy_team_name]", with: "foobar_team", :match => :prefer_exact)
			end

			it "should create a user" do
				expect { click_button submit }.to change(User, :count).by(1)
			end

			describe "after saving a user" do
				before { click_button submit }

  				let(:user) { User.find_by_email("user@Example.com") }
				it { should have_selector('div.alert', text: 'Welcome') }
  				it { should have_link('Sign Out') }
			end
		end
	end

	describe "Edit" do
		let(:user) { FactoryGirl.create(:user) }
		before do
			visit new_user_session_path
			fill_in("user[email]", with: user.email, :match => :prefer_exact)
			fill_in("user[password]", with: user.password, :match => :prefer_exact)
			click_button  "Sign In"
			click_link "Account"
		end

		describe "page" do
			it { should have_title('Edit Account') }
			it { should have_content('Edit Your User Account') }
		end

		describe "updating with invalid info" do
			before { click_button "Update" }

			it { should have_content('Edit Your User Account') }
			it { should have_content('errors') }
		end

		describe "updating username and email with valid info" do
			let(:new_username) { "Newname" }
			let(:new_email) { "Newname@example.com" }
			before do
				fill_in("user[username]", with: new_username, :match => :prefer_exact)
				fill_in("user[email]", with: new_email, :match => :prefer_exact)
				fill_in("user[current_password]", with: user.password, :match => :prefer_exact)
				click_button "Update"
			end

			it { should have_selector('div.alert') }
			it { should have_content('Your account has been updated successfully.') }
			specify { user.reload.username.should == new_username }
			specify { user.reload.email.should 		== new_email.downcase }
		end

		describe "updating password with valid info" do
			let(:new_password) { "foobarbaz" }
			before do
				fill_in("user[password]", with: new_password, :match => :prefer_exact)
				fill_in("user[password_confirmation]", with: new_password, :match => :prefer_exact)
				fill_in("user[current_password]", with: user.password, :match => :prefer_exact)
				click_button "Update"
			end

			it { should have_selector('div.alert') }
			it { should have_content('Your account has been updated successfully') }
			specify { user.reload.password.should == new_password }
		end
	end

	# describe "Subscription Management" do
	# 	let!(:paying_user) { FactoryGirl.create(:paying_user) }

	# 	before do
	# 		sign_in paying_user
	# 		click_link "Account"
	# 	end

		# it { should have_css('a.btn.btn-danger', :text => "Suspend My Subscription") }
		# it { should have_css('a.btn.btn-danger', :text => "Cancel My Subscription") }
		# it { should have_css('a.btn.btn-danger', :text => "Cancel My Account") }
	# end
end
