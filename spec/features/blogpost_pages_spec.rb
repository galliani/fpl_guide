require 'rails_helper'

RSpec.describe "BlogpostPages", :type => :request do

	subject { page }

	let!(:blogpost1) { FactoryGirl.create(:blogpost) }
	let(:admin) 		 { FactoryGirl.create(:admin) }

	describe "Index Page" do
		before(:all) { 20.times { FactoryGirl.create(:blogpost) } }
		after(:all) { Blogpost.delete_all }

		before { visit blogposts_path }

		it { should have_title('Blog') }

		describe "pagination" do
			it { should have_selector('div.pagination') }

			it "should list each blogpost" do
				Blogpost.paginate(page: 1).each do |blogpost|
					page.should have_selector('ul.pagination')
				end
			end
		end
	end

	describe "Show Page" do
		before do 
			sign_in admin
			visit blogpost_path(blogpost1)
		end

		it { should have_title(blogpost1.title) }
		it { should have_content(blogpost1.author) }
		it { should have_content(blogpost1.content) }

		describe "going to Edit Page" do
			before { click_link "Edit" }
			it { should have_content(blogpost1.content) }
		end

		describe "going to index" do
			before { click_link "Index" }

			it { should have_content("FPLGuide Blog") }
		end
	end

	describe "Blogpost creation" do
		before do
			sign_in admin
			visit blogposts_path
			click_link "Create Blog Post"
		end

		it { should have_title("New Blog Post") }

		describe "Create new post with invalid info" do
			it "should not be saved" do
				expect { click_button "Submit Post" }.not_to change(Blogpost, :count)
			end
		end
	end

	describe "Blogpost destruction" do
		before do
			sign_in admin
			visit blogposts_path
		end

		it { should have_link('Destroy', href: blogpost_path(blogpost1)) }

		it "should be able to delete a blogpost" do
			expect { click_link('Destroy') }.to change(Blogpost, :count).by(-1)
		end		
	end

end