require 'rails_helper'

RSpec.describe "MatchPages", :type => :request do

	subject { page }

	let!(:user) { FactoryGirl.create(:user) }
	let!(:admin) { FactoryGirl.create(:admin) }
	let!(:team1) { FactoryGirl.create(:team) }
	let!(:team2) { FactoryGirl.create(:team) }
	let!(:match) { FactoryGirl.create(:incomplete_match, hometeam_id: team1.id, 
											  awayteam_id: team2.id, postmatch: "") }
	let!(:player1) { FactoryGirl.create(:player, team_id: team1.id) }
	let!(:player2) { FactoryGirl.create(:player, team_id: team2.id) }
	let!(:player3) { FactoryGirl.create(:player, team_id: team1.id) }
	let!(:squad_player) { match.squad_players.build(player_id: player3.id) }

	describe "Adding new match" do
		before do 
			sign_in admin
			click_link "Add Match"
		end

		it { should have_title("New Match") }
		it { should have_content("Add New Match") }	
		# it { should have_css("input#match_hometeam_id") }
		# it { should have_css("input#match_awayteam_id") }
		it { should have_css("input#match_gameweek") }
		it { should have_css("textarea#match_prematch") }
		it { should have_css("textarea#match_postmatch") }
		it { should have_link("Back", href: user_root_path) }	
	
		describe "submit with invalid info" do
			before { click_button "Submit Match" }
			it { should have_content("Add New Match") }	
		end
	end

	describe "Show Page" do
		describe "as normal user" do
			before do
				sign_in user
				click_link "Details"
			end

			it { should have_content(team1.name) }
			it { should have_content(team2.name) }
			it { should have_link('Prematch') }
			it { should have_link('Postmatch') }
			it { should have_content(match.prematch) }
			it { should_not have_link('Edit Match') }
			it { should_not have_link('Delete Match') }
			it { should_not have_link('Edit Squad Players') }
			it { should_not have_link('Create Stats') }
		end

		describe "as an admin" do
			before do
				sign_in admin
				click_link "Details"
			end

			it { should have_content(team1.name) }
			it { should have_content(team2.name) }
			it { should have_link('Panel') }
			it { should have_link('Edit Match') }
			it { should have_link('Delete Match') }
			it { should have_link('Edit Squad Players') }
			it { should have_link('Create Stats') }

			it { should have_link(player1.name) }
			it { should have_link(player2.name) }
			it { should have_content(team1.name) }
			it { should have_content(team2.name) }
			it { should have_css("input.btn-success", count: 3) }
			it { should have_css("input.btn-info", count: 3) }
			it { should have_no_link('Recall!') }

			describe "Going into edit page" do
				before { click_link "Edit Match" }

				it { should have_title('Edit Match') }
				it { should have_content('Edit Match') }
				it { should have_link("Back", href: user_root_path) }
			end

			describe "entering postmatch review" do
				before do
					click_link "Edit Match"
					fill_in("match[postmatch]", with: "Lorem Ipsum Dolor Review", :match => :prefer_exact)
					click_button "Submit Match"
				end

				it { should have_content(match.postmatch) }

				describe "creating team stats" do
					before do  
						click_link "Create Stats" 
					end

					it { should have_title("New Team Statistics") }

					describe "filling in with invalid data" do
						it { should have_title("New Team Statistics") }
					end

					describe "filling in with valid data" do
						before do
							options = [[team1.name, team1.id], [team2.name, team2.id]]
							# find(:select, from: '#team_stat_team_id', options).find(:option, "option[1]").select_option
							# select team1.name, :from => "team_stat[team_id]"
							fill_in("team_stat[score]", with: 1, :match => :prefer_exact)
							fill_in("team_stat[concede]", with: 0, :match => :prefer_exact)
							find('#team_stat_point').find(:xpath, 'option[1]').select_option
							fill_in("team_stat[possession]", with: 0.5, :match => :prefer_exact)
							click_button "Submit Stats"
						end

						it { should have_title("Match Detail") }
						it { should have_content(team1.ticker) }
						it { should have_content(1) }
						it { should_not have_content(0) }
						it { should_not have_content(3) }
						it { should have_content(0.5) }
					end
				end
			end

			describe "Going to delete a match" do
				it "should delete a match" do
					expect { click_link "Delete" }.to change(Match, :count).by(-1)
				end
			end


		end



		# describe "Adding Players to the Squad" do
		# 	before { click_link "Manage Home Team Squad" }
		# end		
	end

end	