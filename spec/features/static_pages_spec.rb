require 'rails_helper'

RSpec.describe "StaticPages" do
  
	subject { page }

	let!(:admin) { FactoryGirl.create(:admin) }
	let!(:user1) { FactoryGirl.create(:user) }
	let!(:user2) { FactoryGirl.create(:user) }
	let!(:team1) { FactoryGirl.create(:team) }
	let!(:team2) { FactoryGirl.create(:team) }
	let!(:player1) { FactoryGirl.create(:player, team_id: team1.id) }
	let!(:complete_match) { FactoryGirl.create(:completed_match, hometeam_id: team1.id,
																 awayteam_id: team2.id) }

	describe "Landing Page" do
		before { visit root_path }

		it { should have_content('FAQ') }
		it { should have_title('FPL Guide') }
		it { should_not have_title('| Home') }
	end

	describe "Home as an admin" do
		before do 
			sign_in admin
		end

		it { should have_content(admin.username) }
		it { should have_content('Matches') }
		it { should have_link('Your Fantasy Team') }
		it { should have_link('League Table') }
		it { should have_link('Players') }
		it { should have_link('All Users') }
		it { should have_content(user1.username) }
		it { should have_content(user2.username) }
		it { should have_content(user1.fantasy_team_name) }
		it { should have_content(user2.fantasy_team_name) }
		it { should have_content(team1.name) }
		it { should have_content(player1.name) }
		it { should have_content(team1.ticker) }
		it { should have_content(team2.ticker) }
		it { should have_content(complete_match.gameweek) }
		it { should have_link('Details') }
	end

	describe "Home as regular user" do
		before do 
			sign_in user1
		end

		it { should have_title('| Home') }
		it { should have_content(admin.username) }
		it { should have_content('Matches') }
		it { should have_link('Your Fantasy Team') }
		it { should have_link('League Table') }
		it { should have_link('Players') }
		it { should have_link('All Users') }
		it { should have_content(user1.username) }
		it { should have_content(user2.username) }
		it { should have_content(user1.fantasy_team_name) }
		it { should have_content(user2.fantasy_team_name) }
		it { should have_content(team1.name) }
		it { should have_content(player1.name) }
		it { should have_content(team1.ticker) }
		it { should have_content(team2.ticker) }
		it { should have_content(complete_match.gameweek) }
		it { should have_link('Details') }		
	end
	# describe "Help Page" do
	# 	before { visit help_path }

	# 	it { should have_content('Help') }
	# 	it { should have_title('FPL Guide | Help') }
	# end

end
