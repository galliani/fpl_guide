require 'rails_helper'

RSpec.describe "TeamPages", :type => :request do

	subject { page }

	let!(:user) 	{ FactoryGirl.create(:user) }
	let!(:admin) { FactoryGirl.create(:admin) }
	let!(:user) { FactoryGirl.create(:user) }

	describe "Index Page" do
		let!(:team) { FactoryGirl.create(:team) }
		before do
			sign_in user
			visit teams_path
		end

		it { should have_title('Team Index') }
		it { should have_content(team.name.upcase) }
		it { should_not have_link('Edit') }
		it { should_not have_link('Destroy') }
		it { should_not have_link('New team') }
	end

	describe "Team Creation Page" do
		before do 
			sign_in admin
			visit new_team_path
		end

		it { should have_title('New Team') }

		describe "Create new team with invalid info" do
			it "should be not saved" do
				expect { click_button "Submit" }.not_to change(Team, :count)
			end
		end

		describe "Create new team with valid info" do
		end
	end

	describe "Edit" do
		let!(:team) { FactoryGirl.create(:team) }
		before do
			sign_in admin
			visit edit_team_path(team)
		end

		describe "Page" do
			it { should have_title('Edit Team') }
			it { should have_content('Edit a team') }
		end

		describe "with invalid info" do
			before do
				fill_in("Name", with: " ")
				click_button "Submit" 
			end

			it { should have_content('Edit a team') }
		end

		describe "with valid info" do
			before { click_button "Submit" }

			it { should have_content('Team Listing') }
			it { should have_selector('div.alert') }
		end
	end

	describe "Show page" do
		let!(:team) { FactoryGirl.create(:team) }
		let!(:player) { FactoryGirl.create(:player, team: team) }
		
		before do
			sign_in user
			visit team_path(team)
		end

		it { should have_title(team.name.upcase) }
		it { should have_content('Team Overview') }
		it { should have_content(team.name.upcase) }
		it { should have_content(team.league_position) }
		it { should have_content(team.goals_for) }
		it { should have_content(team.goals_against) }
		it { should have_content(team.goals_for - team.goals_against) }
		it { should have_content("Last 5 Matches") }
		it { should have_content("Next 5 Matches") }
		it { should have_content(team.playing_style) }
		it { should have_content(player.number) }
		it { should_not have_link('Edit player') }
		it { should_not have_link('Delete player') }	
	end

		describe "as an admin" do
			let!(:team) { FactoryGirl.create(:team) }
			let!(:player) { FactoryGirl.create(:player, team: team) }
			before do
				sign_in admin
				visit team_path(team)
			end

			it { should have_link('Edit') }
			it { should have_link('Delete') }
		end

	describe "team deletion as an admin" do
		let!(:team) { FactoryGirl.create(:team) }
		
		before do
			sign_in admin
			visit teams_path
		end

		it { should have_content(team.name.upcase) }
		it { should have_link('Destroy', href: team_path(team)) }
		it "should be able to delete a team" do
			expect { click_link('Destroy') }.to change(Team, :count).by(-1)
		end
	end

end
