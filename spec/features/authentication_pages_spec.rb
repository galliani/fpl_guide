require 'rails_helper'

RSpec.describe "AuthenticationPages", :type => :request do	
	subject { page }

	describe "sign in page" do
		before { visit new_user_session_path }

		it { should have_title('Sign In') }

		describe "when clicking sign up link" do
			before { click_link "Sign Up" }
			it { should have_title('Sign Up') }
	end
	end

	describe "sign in process" do
		before { visit new_user_session_path }
		
		describe "with invalid information" do
			before { click_button "Sign In" }

			it { should have_title('Sign In') }
			it { should have_selector('div.alert', text: 'Invalid') }

			describe "after visiting another page" do
				before { click_link "Home" }
				it { should_not have_selector('div.alert', text: 'Invalid') }
			end
		end

		describe "with valid information" do
			let(:user) { FactoryGirl.create(:user) }
			before { sign_in user }

			it { should have_content('Signed in successfully') }
			it { should have_link('Sign Out', href: destroy_user_session_path) }
			it { should_not have_link('Sign In', href: new_user_session_path) }
			it { should have_selector('div.alert', text: 'Signed in')}
			
			describe "after visiting another page" do
				before { click_link "Blog" }
				it { should_not have_selector('div.alert', text: 'Signed in') }
			end

			# describe "followed by signout" do
			# 	before { click_link "Sign Out" }
			# 	it { should have_link('Sign In') }
			# end				
		end
	end

	describe "authorization" do

		describe "for non-signed-in users" do
			let(:user) { FactoryGirl.create(:user) }
			let(:team) { FactoryGirl.create(:team) }

			describe "in the Teams Controller" do

				describe "visiting the team index page" do
					before { visit teams_path }
					it { should have_title('Sign In') }
				end

				describe "viewing single team (submit to show action)" do
					before { get team_path(team) }
					specify { response.should redirect_to(new_user_session_path) }
				end
			end
		end

		describe "for non-admin users" do
			let!(:user) { FactoryGirl.create(:user) }
			let!(:team) { FactoryGirl.create(:team) }
			let!(:player) { FactoryGirl.create(:player) }
			before { sign_in user }

			describe "in the Teams Controller" do
				describe "visiting the creation page (new)" do
					before { visit new_team_path }
					it { should have_title('Home') }
				end 

				describe "visiting the edit page " do
					before { visit edit_team_path(team) }
					it { should have_title('Home') }
				end

				# describe "submitting to the UPDATE action" do
				# 	before { put team_path(team) }
				# 	specify { response.should redirect_to(user_root_path) }
				# end

				# describe "submitting to the DESTROY action" do
				# 	before { delete team_path(team) }
				# 	specify { response.should redirect_to(user_root_path) }
				# end				
			end

			describe "in the Players Controller" do
				describe "visiting the creation page (new)" do
					before { visit new_player_path }
					it { should have_title('Home') }
				end 

				describe "visiting the edit page " do
					before { visit edit_player_path(player) }
					it { should have_title('Home') }
				end
			end

			# describe "in the Matches Controller" do
			# 	describe "visiting the creation page (new)" do
			# 		before { visit new_match_path }
			# 		it { should have_title('Home') }
			# 	end 

			# 	describe "visiting the edit page " do
			# 		before { visit edit_match_path(match) }
			# 		it { should have_title('Home') }
			# 	end
			# end																							
		end

		describe "for admin users" do
			let!(:admin) { FactoryGirl.create(:admin) }
			let!(:team) { FactoryGirl.create(:team) }	
			let!(:player) { FactoryGirl.create(:player) }	
			before { sign_in admin }

			describe "in the Teams Controller" do
				describe "visiting the creation page (new)" do
					before { visit new_team_path }
					it { should have_title('New Team') }
				end 

				describe "visiting the edit page " do
					before { visit edit_team_path(team) }
					it { should have_title('Edit Team') }
				end				
				
				# describe "submitting to the UPDATE action" do
				# 	before { put team_path(team) }
				# 	specify { response.should redirect_to(user_root_path) }
				# end

				# describe "submitting to the DESTROY action" do
				# 	before { delete team_path(team) }
				# 	specify { response.should redirect_to(user_root_path) }
				# end				
			end

			describe "in the Players Controller" do
				describe "visiting the creation page (new)" do
					before { visit new_player_path }
					it { should have_title('New Player') }
				end 

				describe "visiting the edit page " do
					before { visit edit_player_path(player) }
					it { should have_title('Edit Player') }
				end
			end						
		end
	end
end