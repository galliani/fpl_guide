require 'rails_helper'

RSpec.describe "PlayerPages", :type => :request do

	subject { page }

	let(:user) 		{ FactoryGirl.create(:user) }
	let!(:admin) 	{ FactoryGirl.create(:admin) }
	let!(:team) 	{ FactoryGirl.create(:team) }
	let!(:player) { FactoryGirl.create(:player, team: team) }
	let!(:new_player) { FactoryGirl.create(:player, team: team) }
	let!(:review1) { FactoryGirl.create(:review, player: player) }
	let!(:review2) { FactoryGirl.create(:review, player: player) }

	describe "new player page" do
		before do
			sign_in admin
			click_link "Browse"
			click_link team.name.upcase
			click_link "Add Player"
		end

		it { should have_title('New Player') }
		it { should have_content('Add New Player') }

		describe "Create a new player with invalid info" do
			it "should not be saved" do
				expect { click_button "Submit" }.not_to change(Player, :count)
			end
		end

		describe "Create a new player with valid info" do
			before do
				fill_in("player[name]", with: new_player.name, match: :prefer_exact )
				fill_in("player[team_id]", with: new_player.team_id, match: :prefer_exact )
			end

			it "should be saved" do
				expect { click_button "Submit" }.to change(Player, :count).by(1)
			end
		end
	end

	describe "Show page as an admin" do
		before do
			sign_in admin
			click_link "Browse"
			click_link team.name.upcase
		end

		it { should have_content(player.name) }

		describe "when clicking the name of the player" do
			before { click_link player.name }

			it { should have_title(player.name) }
			it { should have_content(player.name) }
			it { should have_content(player.number) }
			it { should have_content(player.pref_position) }
			it { should have_content(player.playing_roles) }
			it { should have_content(player.appearances) }
			it { should have_content(player.goals) }
			it { should have_content(player.assists) }
			it { should have_content(player.yellow) }
			it { should have_content(player.red) }
			it { should have_content(player.traits) }

			it { should have_content('Timeline') }
			it { should have_link('Edit Player') }
			it { should have_link('Delete Player') }

			describe "it should show reviews of a player" do
				it { should have_content(review1.content) }
				it { should have_content(review1.title) }
				it { should have_content(review2.content) }
				it { should have_content(review2.title) }
			end

			describe "it should show link to post a review" do
				it { should have_link('Post review') }
			end

			describe "Edit page" do
				before { click_link "Edit Player" }

				it { should have_title('Edit Player') }
				it { should have_content('Edit Player') }
			end
		end
	end

	describe "show page as non-admin" do
		before do
			sign_in user
			visit team_path(team)
			click_link player.name
		end

		it { should_not have_link('Edit Player') }
		it { should_not have_link('Delete Player') }
		it { should_not have_link('Post review') }
	end	
end
