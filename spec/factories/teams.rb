# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  sequence(:count)
  
  factory :team do
    sequence(:name) { |n| "Team#{n}" }
    sequence(:league_position) { |n| "#{n}" }
    sequence(:ticker) { |n| "FC#{n}" }
    goals_for 51
    goals_against 20
    playing_style "balanced"
  end

  factory :player do
    sequence(:name) { |n| "Michael #{n}" }
    sequence(:number) { |n| "#{n}" }
    pref_position "Winger"
    playing_roles "Playmaker"
    appearances 7
    goals	2
    assists 5
    yellow 3
    red 0
    traits "Creative, Trickster, Passer"
    team
  end

  factory :review do
    sequence(:title) { |n| "Review no.#{n}" }
    rating 8
    sequence(:content) { |n| "Lorem Ipsum Dolor no.#{n}" }
    player
  end


end
