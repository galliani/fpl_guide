FactoryGirl.define do

	factory :match do
		gameweek 1
		kickoff "2014-10-24 15:15:00"

		factory :incomplete_match do
			prematch "Preview: Lorem Ipsum Dolor"
		end

		factory :completed_match do
			prematch "Preview: Lorem Ipsum Dolor"
			postmatch "Review: Lorem Ipsum Dolor"
		end
	end
end
