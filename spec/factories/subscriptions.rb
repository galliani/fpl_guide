FactoryGirl.define do
  factory :subscription do
    plan 
    user
	email "paid_user@example.com"
	paypal_customer_token "MyPaypalCustomerTokenString"
	paypal_recurring_profile_token "MyPaypalRecurringProfileTokenString"

	factory :active_subscription do
		status "active"
	end

	factory :suspended_subscription do
		status "suspended"
	end

	factory :cancelled_subscription do
		status "cancelled"
	end
  end
end
