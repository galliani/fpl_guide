FactoryGirl.define do
  factory :blogpost do
    sequence(:title) { |n| "Blog Post No.#{n}"}
    author "Michael"
    content "Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor"
  end

end
