FactoryGirl.define do
  factory :message do
    name "Michael"
    email "michael@example.com"
    subject "Feedback"
    body "Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor "
  end

end
