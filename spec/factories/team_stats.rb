FactoryGirl.define do
  
  factory :team_stat do
    sequence(:on_target) { |n| n + 1 }
    sequence(:offside) { |n| n + 1 }
    sequence(:foul) { |n| n + 1 }
    sequence(:yellow_card) { |n| n + 1 }
    sequence(:red_card) { |n| n + 1 }
  end
end