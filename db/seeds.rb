# Examples:
#
#   cities = City.create([{ {name: 'Chicago' }, { {name: 'Copenhagen' }])
#   Mayor.create({name: 'Emanuel', city: cities.first)
# 
if Rails.env.development?
	# Create admin with seeds
	admin_user = User.new( email: "michael@example.com", user{name: "michael",
						   password: "asdasdasd", password_confirmation: "asdasdasd", 
						   fantasy_team_{name: "admin_team" )
	admin_user.admin = true
	admin_user.save!

	
	(1..30).each do |n|
		User.create!( email: "user#{n}@example.com", username: "udummy#{n}", 
					  password: "asdasdasd", password_confirmation: "asdasdasd",
					  fantasy_team_name: "#{n}th_team" )
	end

	Team Seeds
	team_array =[]
	(1..20).each do |t|
	  team_array <<  Team.create!(name: "#{t}. Club FC", ticker: "CL#{t}")
	end

	# Player seeds
	# project_array.each do |project|                                            
	#   (1..20).each do |num|                                                    
	#     Task.create!(description: "Task#{num}", project_id: project.id)        
	#   end                                                                      
	# end      
	team_array.each do |team|
		(1..20).each do |player|
			Player.create!(name: "Player_#{player}", team_id: team.id)
		end
	end
end

# if Rails.env.production?
# 	admin_user = User.new( email: "galih0muhammad@gmail.com", username: "galliani",
# 						   password: "asdasdasd", 
# 						   password_confirmation: "asdasdasd" )
# 	admin_user.admin = true
# 	admin_user.save!

# 	arsenal = Team.create!(name: "Arsenal", ticker: "ARS")
# 	villa = Team.create!(name: "Aston Villa", ticker: "AVL")
# 	burnley = Team.create!(name: "Burnley", ticker: "BUR")
# 	chelsea = Team.create!(name: "Chelsea", ticker: "CHE")
# 	palace = Team.create!(name: "Crystal Palace", ticker: "CRY")
# 	everton = Team.create!(name: "Everton", ticker: "EVE")
# 	hull = Team.create!(name: "Hull City", ticker: "HUL")
# 	leicester = Team.create!(name: "Leicester City", ticker: "LEI")
# 	liverpool = Team.create!(name: "Liverpool", ticker: "LIV")
# 	mancity = Team.create!(name: "Manchester City", ticker: "MCI")
# 	manutd = Team.create!(name: "Manchester United", ticker: "MCU")
# 	newcastle = Team.create!(name: "Newcastle United", ticker: "NCU")
# 	rangers = Team.create!(name: "Queens Park Rangers", ticker: "QPR")
# 	soton = Team.create!(name: "Southampton", ticker: "SOU")
# 	stoke = Team.create!(name: "Stoke City", ticker: "STO")
# 	sunderland = Team.create!(name: "Sunderland", ticker: "SUN")
# 	swansea = Team.create!(name: "Swansea City", ticker: "SWA")
# 	spurs = Team.create!(name: "Tottenham Hotspur", ticker: "TOT")
# 	westbrom = Team.create!(name: "West Bromwich Albion", ticker: "WBA")
# 	westham = Team.create!(name: "West Ham United", ticker: "WHU")

# 	# Arsenal
# 	Player.create({name: "Wojciech Scezczny", number: 1, pref_position: "Goalkeeper", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Mathieu Debuchy", number: 2, pref_position: "Full-back", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Kieran Gibbs", number: 3, pref_position: "Full-back", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Per Mertesacker", number: 4, pref_position: "Central Defender", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Laurent Koscielny", number: 6, pref_position: "Central Defender", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Mikel Arteta", number: 8, pref_position: "Defensive Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Aaron Ramsey", number: 16, pref_position: "Central Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Santi Cazorla", number: 19, pref_position: "Attacking Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Alexis Sancehz", number: 17, pref_position: "Winger", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Alex Oxlade-Chamberlain", number: 15, pref_position: "Winger", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Olivier Giroud", number: 12, pref_position: "Forward", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "David Ospina", number: 13, pref_position: "Goalkeeper", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Emiliano Martinez", number: 26, pref_position: "Goalkeeper", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Calum Chambers", number: 21, pref_position: "Full-back", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Nacho Monreal", number: 18, pref_position: "Full-back", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Gabriel Paulista", number: 5, pref_position: "Central Defender", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Hector Bellerin", number: 39, pref_position: "Full-back", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Isaac Hayden", number: 42, pref_position: "Central Defender", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Tomas Rosicky", number: 7, pref_position: "Atacking Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Jack Wilshere", number: 10, pref_position: "Central Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Mesut Ozil", number: 11, pref_position: "Atacking Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Mathieu Flamini", number: 20, pref_position: "Defensive Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Abou Diaby", number: 24, pref_position: "Central Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Francis Coquelin", number: 34, pref_position: "Defensive Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Gedion Zelalem", number: 35, pref_position: "Central Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Krytian Bielik", number: 36, pref_position: "Defensive Midfielder", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Danny Welbeck", number: 23, pref_position: "Forward", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Serge Gnabry", number: 27, pref_position: "Winger", team_id: arsenal.id}, without_protection: true )
# 	Player.create({name: "Chuba Akpom", number: 38, pref_position: "Forward", team_id: arsenal.id}, without_protection: true )

# 	# Villa
# 	Player.create({name: "Brad Guzan", number: 1, pref_position: "Goalkeeper")
# 	Player.create({name: "Jed Steer", number: 13, pref_position: "Goalkeeper")
# 	Player.create({name: "Shay Given", number: 31, pref_position: "Goalkeeper")
# 	Player.create({name: "Nathan Baker", number: 2, pref_position: "Central Defender")
# 	Player.create({name: "Alan Hutton", number: 21, pref_position: "Full-back")
# 	Player.create({name: "Joe Bennet", number: 3, pref_position: "Full-back")
# 	Player.create({name: "Ron Vlaar", number: 4, pref_position: "Central Defender")
# 	Player.create({name: "Jores Okore", number: 5, pref_position: "Central Defender")
# 	Player.create({name: "Ciaran Clark", number: 6, pref_position: "Central Defender")
# 	Player.create({name: "Philippe Senderos", number: 14, pref_position: "Central Defender")
# 	Player.create({name: "Kieran Richardson", number: 18, pref_position: "Full-back")
# 	Player.create({name: "Aly Cissokho", number: 23, pref_position: "Full-back")
# 	Player.create({name: "Matthew Lowton", number: 34, pref_position: "Full-back")
# 	Player.create({name: "Leandro Bacuna", number: 7, pref_position: "Central Midfielder")
# 	Player.create({name: "Tom Cleverley", number: 8, pref_position: "Central Midfielder")
# 	Player.create({name: "Joe Cole", number: 12, pref_position: "Attacking Midfielder")
# 	Player.create({name: "Ashley Westwood", number: 15, pref_position: "Defensive Midfielder")
# 	Player.create({name: "Fabian Delph", number: 16, pref_position: "Central Midfielder")
# 	Player.create({name: "Carlos Sanchez", number: 24, pref_position: "Central Midfielder")
# 	Player.create({name: "Charles Gil", number: 25, pref_position: "Attacking Midfielder")
# 	Player.create({name: "Charles N'Zogbia", number: 28, pref_position: "Winger")
# 	Player.create({name: "Scott Sinclaier", number: 9, pref_position: "Winger")
# 	Player.create({name: "Andreas Weimann", number: 10, pref_position: "Forward")
# 	Player.create({name: "Gabriel Agbonlahor", number: 11, pref_position: "Forward")
# 	Player.create({name: "Darren Bent", number: 19, pref_position: "Forward")
# 	Player.create({name: "Christian Benteke", number: 20, pref_position: "Forward")
# 	Player.create({name: "Libor Kozak", number: 27, pref_position: "Forward")

# end	