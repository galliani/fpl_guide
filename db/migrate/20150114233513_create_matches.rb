class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.references	:hometeam, null: false
      t.references	:awayteam, null: false
      t.integer		:gameweek
      t.datetime	:kickoff
      t.text		:prematch, default: "" 
      t.text		:postmatch, default: "" 
      t.timestamps
    end
    add_index :matches, :gameweek
    add_index :matches, :hometeam_id
    add_index :matches, :awayteam_id 
    add_index :matches, [:hometeam_id, :awayteam_id], uniqueness: true    
  end
end
