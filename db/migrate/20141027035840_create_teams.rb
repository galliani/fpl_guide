class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string   :name, null: false, default: ""
      t.string   :ticker, null: false, default: ""
      t.integer  :league_position
      t.integer  :league_points, default: 0
      t.integer  :won, default: 0
      t.integer  :drawn, default: 0
      t.integer  :lost, default: 0
      t.integer	 :goals_for, default: 0
      t.integer	 :goals_against, default: 0
      t.integer  :goal_difference
      t.integer  :attempts, default: 0
      t.integer  :on_targets, default: 0
      t.integer  :offsides, default: 0
      t.integer  :yellows, default: 0
      t.integer  :reds, default: 0
      t.decimal  :possession
      t.string	 :league_form, default: ""
      t.string	 :next_opps, default: ""
      t.text	   :playing_style, default: ""
      t.string   :photo_file_name
      t.string   :photo_content_type
      t.integer   :photo_file_size
      t.datetime  :photo_updated_at

      t.timestamps
    end
  end
end
