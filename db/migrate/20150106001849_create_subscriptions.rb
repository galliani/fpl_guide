class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.belongs_to :plan, null: false
      t.belongs_to :user, null: false
      t.string :email
      t.string :paypal_customer_token
      t.string :paypal_recurring_profile_token
      t.string :status, default: "" 

      t.timestamps
    end
    add_index :subscriptions, :plan_id
    add_index :subscriptions, :user_id
  end
end
