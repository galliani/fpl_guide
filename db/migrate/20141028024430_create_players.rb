class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string 	 :name, null: false, default: "" 
      t.integer  :number, default: 1
      t.string 	 :pref_position, default: "" 
      t.string 	 :playing_roles, default: "" 
      t.integer  :appearances, default: 0
      t.integer  :substitutes, default: 0
      t.integer  :goals, default: 0
      t.integer  :assists, default: 0
      t.integer  :yellow, default: 0
      t.integer  :red, default: 0
      t.integer  :tot_points, default: 0
      t.text     :traits
      t.references :team
      t.string   :photo_file_name
      t.string   :photo_content_type
      t.integer   :photo_file_size
      t.datetime   :photo_updated_at

      t.timestamps
    end
    add_index :players, :team_id
  end
end