class CreateBlogposts < ActiveRecord::Migration
  def change
    create_table :blogposts do |t|
    	t.string 	:title, null: false
    	t.string 	:author
    	t.text 		:content, null: false
    	
      t.timestamps
    end
  end
end
