class CreateSquadPlayers < ActiveRecord::Migration
  def change
    create_table :squad_players do |t|
		t.integer :match_id, null: false
    	t.integer :player_id, null: false
        t.integer :gameweek
    	t.boolean :starting, default: false
    	t.integer :lasting, default: 0
    	t.integer :scored, default: 0
    	t.integer :assisted, default: 0
    	t.boolean :cautioned, default: false
    	t.boolean :sent_off, default: false
    	t.integer :rating, default: 6
        t.integer :point_gained, default: 0
      t.timestamps
    end
    add_index :squad_players, :match_id
    add_index :squad_players, :gameweek
    add_index :squad_players, :player_id
    add_index :squad_players, [:match_id, :player_id], uniqueness: true
  end
end
