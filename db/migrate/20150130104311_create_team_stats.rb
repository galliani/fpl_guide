class CreateTeamStats < ActiveRecord::Migration
  def change
  	create_table :team_stats do |t|
  		t.references :match, null: false
  		t.references :team, null: false
  		t.integer :point
  		t.integer :score
      t.integer :concede
  		t.integer :shot, default: 0
  		t.integer :on_target, default: 0
  		t.integer :offside, default: 0
  		t.integer :foul, default: 0
  		t.integer :yellow_card, default: 0
  		t.integer :red_card, default: 0
  		t.decimal :possession
  	end
  	add_index :team_stats, :match_id
  	add_index :team_stats, :team_id
  	add_index :team_stats, [:match_id, :team_id], uniqueness: true 
  end
end
