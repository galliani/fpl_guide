class CreateLineups < ActiveRecord::Migration
  def change
    create_table :lineups do |t|
      t.integer :player_id, null: false
      t.integer :user_id, null: false
      t.integer :gameweek
      t.integer :point
      t.string  :role, default: "squad"
      t.timestamps
    end
    add_index :lineups, :player_id
    add_index :lineups, :user_id 
    add_index :lineups, [:player_id, :user_id], uniqueness: true
  end
end
