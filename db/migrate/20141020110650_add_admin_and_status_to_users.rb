class AddAdminAndStatusToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :admin, :boolean, :default => false
    add_column :users, :status, :string, :default => "trial"
    add_column :users, :fantasy_team_name, :string
    add_column :users, :photo_file_name, :string
    add_column :users, :photo_content_type, :string
    add_column :users, :photo_file_size, :integer
    add_column :users, :photo_updated_at, :datetime
  end

  def self.down
  	remove_column :users, :admin
  	remove_column :users, :status
  	remove_column :users, :fantasy_team_name
    remove_column :users, :photo_file_name
    remove_column :users, :photo_content_type
    remove_column :users, :photo_file_size
    remove_column :users, :photo_updated_at  	
  end
end
