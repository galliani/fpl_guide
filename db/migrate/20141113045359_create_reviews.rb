class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :title, default: "" 
      t.text :content, default: "" 
      t.integer :rating, default: 6
      t.references :player
      t.references :match

      t.timestamps
    end
    add_index :reviews, :player_id
    add_index :reviews, [:player_id, :match_id]
  end
end
