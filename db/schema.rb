# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150130104311) do

  create_table "blogposts", :force => true do |t|
    t.string   "title",      :null => false
    t.string   "author"
    t.text     "content",    :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "lineups", :force => true do |t|
    t.integer  "player_id",                       :null => false
    t.integer  "user_id",                         :null => false
    t.integer  "gameweek"
    t.integer  "point"
    t.string   "role",       :default => "squad"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "lineups", ["player_id", "user_id"], :name => "index_lineups_on_player_id_and_user_id"
  add_index "lineups", ["player_id"], :name => "index_lineups_on_player_id"
  add_index "lineups", ["user_id"], :name => "index_lineups_on_user_id"

  create_table "matches", :force => true do |t|
    t.integer  "hometeam_id",                 :null => false
    t.integer  "awayteam_id",                 :null => false
    t.integer  "gameweek",    :default => 1
    t.datetime "kickoff"
    t.text     "prematch",    :default => ""
    t.text     "postmatch",   :default => ""
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  add_index "matches", ["awayteam_id"], :name => "index_matches_on_awayteam_id"
  add_index "matches", ["gameweek"], :name => "index_matches_on_gameweek"
  add_index "matches", ["hometeam_id", "awayteam_id"], :name => "index_matches_on_hometeam_id_and_awayteam_id"
  add_index "matches", ["hometeam_id"], :name => "index_matches_on_hometeam_id"

  create_table "plans", :force => true do |t|
    t.string   "name"
    t.decimal  "price"
    t.integer  "duration"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "players", :force => true do |t|
    t.string   "name",               :default => "", :null => false
    t.integer  "number",             :default => 1
    t.string   "pref_position",      :default => ""
    t.string   "playing_roles",      :default => ""
    t.integer  "appearances",        :default => 0
    t.integer  "substitutes",        :default => 0
    t.integer  "goals",              :default => 0
    t.integer  "assists",            :default => 0
    t.integer  "yellow",             :default => 0
    t.integer  "red",                :default => 0
    t.integer  "tot_points",         :default => 0
    t.text     "traits"
    t.integer  "team_id"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "players", ["team_id"], :name => "index_players_on_team_id"

  create_table "reviews", :force => true do |t|
    t.string   "title",      :default => ""
    t.text     "content",    :default => ""
    t.integer  "rating",     :default => 6
    t.integer  "player_id"
    t.integer  "match_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "reviews", ["player_id", "match_id"], :name => "index_reviews_on_player_id_and_match_id"
  add_index "reviews", ["player_id"], :name => "index_reviews_on_player_id"

  create_table "squad_players", :force => true do |t|
    t.integer  "match_id",                        :null => false
    t.integer  "player_id",                       :null => false
    t.integer  "gameweek"
    t.boolean  "starting",     :default => false
    t.integer  "lasting",      :default => 0
    t.integer  "scored",       :default => 0
    t.integer  "assisted",     :default => 0
    t.boolean  "cautioned",    :default => false
    t.boolean  "sent_off",     :default => false
    t.integer  "rating",       :default => 6
    t.integer  "point_gained", :default => 0
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "squad_players", ["gameweek"], :name => "index_squad_players_on_gameweek"
  add_index "squad_players", ["match_id", "player_id"], :name => "index_squad_players_on_match_id_and_player_id"
  add_index "squad_players", ["match_id"], :name => "index_squad_players_on_match_id"
  add_index "squad_players", ["player_id"], :name => "index_squad_players_on_player_id"

  create_table "subscribers", :force => true do |t|
    t.string   "email"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "subscriptions", :force => true do |t|
    t.integer  "plan_id",                                        :null => false
    t.integer  "user_id",                                        :null => false
    t.string   "email"
    t.string   "paypal_customer_token"
    t.string   "paypal_recurring_profile_token"
    t.string   "status",                         :default => ""
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  add_index "subscriptions", ["plan_id"], :name => "index_subscriptions_on_plan_id"
  add_index "subscriptions", ["user_id"], :name => "index_subscriptions_on_user_id"

  create_table "team_stats", :force => true do |t|
    t.integer "match_id",                   :null => false
    t.integer "team_id",                    :null => false
    t.integer "point"
    t.integer "score"
    t.integer "concede"
    t.integer "shot",        :default => 0
    t.integer "on_target",   :default => 0
    t.integer "offside",     :default => 0
    t.integer "foul",        :default => 0
    t.integer "yellow_card", :default => 0
    t.integer "red_card",    :default => 0
    t.decimal "possession"
  end

  add_index "team_stats", ["match_id", "team_id"], :name => "index_team_stats_on_match_id_and_team_id"
  add_index "team_stats", ["match_id"], :name => "index_team_stats_on_match_id"
  add_index "team_stats", ["team_id"], :name => "index_team_stats_on_team_id"

  create_table "teams", :force => true do |t|
    t.string   "name",               :default => "", :null => false
    t.string   "ticker",             :default => "", :null => false
    t.integer  "league_position"
    t.integer  "league_points",      :default => 0
    t.integer  "won",                :default => 0
    t.integer  "drawn",              :default => 0
    t.integer  "lost",               :default => 0
    t.integer  "goals_for",          :default => 0
    t.integer  "goals_against",      :default => 0
    t.integer  "goal_difference"
    t.integer  "attempts",           :default => 0
    t.integer  "on_targets",         :default => 0
    t.integer  "offsides",           :default => 0
    t.integer  "yellows",            :default => 0
    t.integer  "reds",               :default => 0
    t.decimal  "possession"
    t.string   "league_form",        :default => ""
    t.string   "next_opps",          :default => ""
    t.text     "playing_style",      :default => ""
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "username"
    t.string   "email",                  :default => "",      :null => false
    t.string   "encrypted_password",     :default => "",      :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,       :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.boolean  "admin",                  :default => false
    t.string   "status",                 :default => "trial"
    t.string   "fantasy_team_name"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
