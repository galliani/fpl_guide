namespace :db do
	desc "Fill database with sample data"
	task populate: :environment do
		make_reviews
	end	
end

def make_users
	20.times do |n|
		username = Faker::Name.name
		email = "test-#{n+1}@railstutorial.org"
		password = "password"
		User.create!(username: username,
								 email: email,
								 password: password,
								 password_confirmation: password)
	end
end

def make_teams
	20.times do |n|
		name = "Team_#{n+1}_FC"
		league_position = n+1
		goals_for = "51"
    goals_against = "20"
    league_form = "WWWWW"
    next_opps = "Team_A_FC", "Team_B_FC", "Team_C_FC", "Team_D_FC", "Team_#E_FC" 
    playing_style = "balanced"
		Team.create!(name: name, league_position: league_position,
								 goals_for: goals_for, goals_against: goals_against,
								 league_form: league_form, next_opps: next_opps,
								 playing_style: playing_style)
	end		
end

def make_goalkeepers
	teams = Team.all
	2.times do |n|
		name = "Goalie_#{n+1}"
		number = n+81
		pref_position = "Goalkeeper"
    playing_roles = "Sweeper Keeper"
    appearances = 10
    goals = 0 
    assists = 0
    yellow = 0
    red = 0
    traits = "balanced"
		teams.each { |team| team.players.create!(name: name, number: number,
								 pref_position: pref_position, playing_roles: playing_roles,
								 appearances: appearances, goals: goals, assists: assists,
								 yellow: yellow, red: red, traits: traits) }
	end		
end

def make_defenders
	teams = Team.all
	6.times do |n|
		name = "Defender_#{n+1}"
		number = n+4
		pref_position = "Defender"
    playing_roles = "Stopper"
    appearances = 10
    goals = 0 
    assists = 0
    yellow = 3
    red = 1
    traits = "balanced"
		teams.each { |team| team.players.create!(name: name, number: number,
								 pref_position: pref_position, playing_roles: playing_roles,
								 appearances: appearances, goals: goals, assists: assists,
								 yellow: yellow, red: red, traits: traits) }
	end		
end

def make_midfielders
	teams = Team.all
	6.times do |n|
		name = "Midfield_#{n+1}"
		number = n+10
		pref_position = "Winger"
    playing_roles = "Playmaker"
    appearances = 10
    goals = 2 
    assists = 6
    yellow = 1
    red = 0
    traits = "balanced"
		teams.each { |team| team.players.create!(name: name, number: number,
								 pref_position: pref_position, playing_roles: playing_roles,
								 appearances: appearances, goals: goals, assists: assists,
								 yellow: yellow, red: red, traits: traits) }
	end		
end

def make_forwards
	teams = Team.all
	3.times do |n|
		name = "Forward_#{n+1}"
		number = n+23
		pref_position = "Forward"
    playing_roles = "Advanced Forward"
    appearances = 10
    goals = 6 
    assists = 2
    yellow = 1
    red = 0
    traits = "balanced"
		teams.each { |team| team.players.create!(name: name, number: number,
								 pref_position: pref_position, playing_roles: playing_roles,
								 appearances: appearances, goals: goals, assists: assists,
								 yellow: yellow, red: red, traits: traits) }
	end		
end

def make_reviews
	players = Player.all
	4.times do |n|
		title = "Review no.#{n+1}"
		rating = 7
		content = Faker::Lorem.sentence(5)
		players.each { |player| player.reviews.create!(title: title, 
									 rating: rating, content: content) }
	end		
end